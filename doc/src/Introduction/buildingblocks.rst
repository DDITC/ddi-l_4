DDI Base Blocks
================

Complex Data Types
-------------------

These are extensions of base type Primitives
All complex data types (that is, the set of complex structures which are treated within the Drupal modeling platform as primitives, as for the values of properties) are located in the Complex Data Types package. There is a distinct style of modeling these: each complex data type which has a primary content will have a property named “content” of whatever primitive type is needed. Complex data types will not be extensions of the primitive type of their primary content.
[A set of complex structures which are used as datatypes for properties within a class description. There is a distinct style of modeling these: each complex data type which has a primary content will have a property named “content” of whatever primitive type is needed. Complex data types will not be extensions of the primitive type of their primary content. This allows standard structures, such as capturing structured language strings, to be expressed in a single way throughout DDI.]

Relationships
--------------

DDI classes are associated via binary relationships. Relationships have cardinality constraints, e.g. 1..1, 1..n, 0..n, and can be of different types, i.e. aggregation, composition, and neither (simple, untyped). Even though most relationships in the model are undirected, in the Drupal they are always defined within one of the classess participating in the relationship, i.e. the source; the class at the other end of the relationship is called the target. Similarly with names: the predicate represented in a relationship name does not impose a direction since the implicit converse predicate is also true in all undirected relationships. It’s important to note that this is just a convention used in the Drupal and by no means imposes an actual conceptual direction in the association.
For instance, RepresentedVariable has a relationship with ValueDomain. The relationship is defined in ValueDomain, which is the source, and it is named by a predicate, i.e. takesValuesFrom. This seems to imply a direction from RepresentedVariable to ValueDomain. However, the converse predicate, i.e. providesValuesFor, although not represented in the model, is also valid since such a relationship is conceptually undirected. In general, and unless otherwise indicated, all relationships in the Drupal are undirected.

Identification (Identifiable class)
------------------------------------

The Identifiable class is core to the functioning of the standard. The purpose of the DDI Identifiable class is to:
* Uniquely identify major objects in a persistent manner to ensure accurate reference and retrieval of the object content
* Provide context for classes where an understanding of related class types within a packaging structure is essential to the understanding of the class (i.e., a specific classification within a classification scheme)
* Manage metadata object change over time
* Support the range of object management used by different organizations
* Support early and late binding of references
* Support interaction with closely related standards, in particular ISO/IEC 11179 and SDMX

Many organizations may have preexisting URI schemes, or have URI patterns imposed on them other organizations or governments. Unlike DDI3.x DDI4 will not require any specific information or pattern to be contained in the URIs of described resource.

To align with both DDI 3.x and ISO/IEC 11179-6, the Identifiable object will continue to be based on a combination of:
* Agency Identifier (a unique identifier for the agency managing the object)
* Item Identifier (a unique identifier of the object within the context of the agency)
* Item Version (a version number of the object to track change over time)

These parts correspond to the agency, id, and version used in DDI 3.x and to the registration authority identifier (RAI), data identifier (DI), and version identifier (VI) constituting the international registration data identifier (IRDI) in ISO/IEC 11179-6

In DDI 3.x, all items had an agency, item id, and version. However, some types of items could inherit a parent item’s agency. Some items would inherit a parent item’s version. In DDI 4, all items will have their own Agency, Item Identifier, and Item Version specified.
This three part structure is the equivalent of a unique persistent identifier for an object, such as described by DOIs and other similar structures. Note that while use of a version number with a DOI is optional, based on local practice, the Version Number in DDI is required due to the need to manage metadata within a dynamic workflow over time

Character Restrictions
-----------------------

In DDI 3.x, regular expressions restricted the Agency Identifier, Item Identifier, and Item Version. This has been removed for DDI 4. The only restrictions are that it is a string without colons and whitespace.
Note that these are restrictions on the specific content not the structure of a DDI URN. The restriction on the use of a colon supports the use of this character as a URN separator. This complies with ISO/IEC 11179-6 as it imposes no limitations on the contents of the IRDI fields.
In DDI 3.2, versions are restricted to integers that may be separated by periods. This forces implementers to use a specific versioning system. A more flexible system would use a “based on” reference to determine version history. In addition, a “based on” system adds the ability to branch and merge. A Based On system would be backwards compatible with DDI 3.x versioning systems.

