Design Principles
==================

A set of design principles has been identified during the course of the development of DDI4, The list is shown below:

===============================  ================================================================================================================================================
Principle	                     Definition
===============================  ================================================================================================================================================
Interoperability and Standards	 The model is optimized to facilitate interoperability with other relevant standards
Simplicity	                     The model is as simple as possible and easily understandable by different stakeholders
User Driven	                     User perspectives inform the model to ensure that it meets the needs of the international DDI user community
Terminology	                     The model uses clear terminology and when possible, uses existing terms and definitions
Iterative Development	         The model is developed iteratively, bringing in a range of views from the user community
Documentation	                 The model includes and is supplemented by robust and accessible documentation
Lifecycle Orientation	         The model supports the full research data lifecycle and the statistical production process, facilitating replication and the scientific method
Reuse and Exchange	             The model supports the reuse, exchange, and sharing of data and metadata within and among institutions
Modularity	                     The model is modular and these modules can be used independently
Stability	                     The model is stable and new versions are developed in a controlled manner
Extensibility	                 The model has a common core and is extensible
Tool Independence	             The model is not dependent on any specific IT setting or tool
Innovation	                     The model supports both current and new ways of documenting, producing, and using data and leverages modern technologies
Actionable Metadata	             The model provides actionable metadata that can be used to drive production and data collection processes
===============================  ================================================================================================================================================

Additional lower-level principles have surfaced during initial DDI model development:

==================================  ============================================================================================================================================================================================
Principle	                        Definition
==================================  ============================================================================================================================================================================================
Remodelling Discouraged	            The model leverages existing structures in the specification whenever possible to avoid inefficiencies
Classes Represent Actual Things	    The model includes classes that are functional and are used
Separation of Logical and Physical	The model supports a distinction between logical and physical representations
Names are Mutable	                The model contains names and labels that may change to encourage accessibility
Common Expressions	                The model will only have features that reflect the common expressive capabilities of supported syntaxes/technologies (e.g., no multiple inheritances)
==================================  ============================================================================================================================================================================================

