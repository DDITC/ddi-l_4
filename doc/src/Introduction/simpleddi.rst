Simple example
==============

For documenting a dataset the content could look something like this.

===  =======  ======
ID   subj     q1
===  =======  ======
0    Anders   3
1    Lars     2
2    Krister  3
===  =======  ======

A DDI Example
--------------

.. code-block:: xml

   <DDI>
    <DataFile>
      <path>ct-2015.survey.csv</path>
      <haveLogicalDataset>dataset1</haveLogicalDataset>
    </DataFile>
    <LogicalDataset id="dataset1">
      <hasVariable>ID</hasVariable>
      <hasVariable>subj</hasVariable>
      <hasVariable>q1</hasVariable>
    </LogicalDataset>
    <Variable id="ID">
      <Label>The record id of the collection</Label>
      <Type>Numreric</Type>
    </Variable>
    <Variable id="subj">
      <Label>Person interviewd</Label>
      <Type>String</Type>
    </Variable>
    <Variable id="q1">
      <Label>Relation to cats</Label>
      <hasCodeList>c_list_1</hasCodeList>
    </Variable>
    <CodeList id="c_list_1">
      <hasCode>c2</hasCode>
      <hasCode>c3</hasCode>
    </CodeList>
    <Code id="c2">
      <value>2</value>
      <Label>Yes</Label>
    </Code>
    <Code id="c3">
      <value>3</value>
      <label>No</label>
    </Code>
   </DDI>
