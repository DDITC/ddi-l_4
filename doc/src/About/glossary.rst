.. _Glossary:

********
Glossary
********

.. glossary::

   Abstract class
      In programming languages, an abstract type is a type in a nominative type system which cannot be instantiated directly.
      Its only purpose is for other classes to extend (see http://en.wikipedia.org/wiki/Abstract_type).

   Binding
      Data binding is a way to un/serialize objects across programs, languages, and platforms.
      The structure and the data remain consistent and coherent throughout the journey, and no custom formats
      or parsing are required (see http://en.wikipedia.org/wiki/XML_data_binding).

   Canonical
      Conforming to a general rule or acceptable procedure (see http://www.merriam-webster.com/dictionary/canonical).

   Codebook
      A codebook is a type of document used for gathering and storing codes. Originally codebooks were often literally books,
      but today codebook is a byword for the complete record of a series of codes, regardless of physical format (see  http://en.wikipedia.org/wiki/Codebook).
      DDI Codebook is the development line of the DDI specification that reflects the content of codebooks.

   Class
     In the DDI model, as in software engineering, a class diagram in the Unified Modeling Language (UML) is a type of static structure
     diagram that describes the structure of a system by showing the system's classes, their attributes, operations (or methods),
     and the relationships among objects, (see https://en.wikipedia.org/wiki/Class_diagram).

   Content Capture
      Content capture refers to the process of harvesting content from other sources.
      For DDI 4 development, content is being captured using the Drupal content management system to permit machine-processing.

   Content Modeler
      One of the group of people determining the requirements for a Functional View and then identifying the set of objects
      needed to meet those requirements. In this process they may also need to describe new objects.

   Data Modeler
      These people work with Content Modelers to insure that new objects are consistent with the objects accepted into the approved model.
      Data Modelers also arrange objects into the final namespace structure of the published model.

   DDI
      The Data Documentation Initiative (DDI) is an effort to create an international standard for describing data from the social,
      behavioral, and economic sciences. The DDI metadata specification now supports the entire research data life cycle.
      DDI metadata accompanies and enables data conceptualization, collection, processing, distribution, discovery, analysis,
      repurposing, and archiving.

   Drupal
      Drupal is a free and open-source content management framework written in PHP and distributed under the GNU General Public License.
      It is also used for knowledge management and business collaboration (see http://en.wikipedia.org/wiki/Drupal).

   Enterprise Architect
      Enterprise Architect is a visual modeling and design tool based on the OMG UML. The platform supports: the design and
      construction of software systems; modeling business processes; and modeling industry based domains.
      It is used by businesses and organizations to not only model the architecture of their systems, but to process the
      implementation of these models across the full application development life-cycle (see http://en.wikipedia.org/wiki/Enterprise_Architect_(Visual_Modeling_Platform).

   Extended Primitive
      An extended data type is a user-defined definition of a primitive data type.
      The following primitive data types can be extended: boolean, integer, real, string, date and
      container (see http://www.axaptapedia.com/Extended_Data_Types).

   Extension
      Extension is the inheritance of one object’s properties and relationships from another object.
      It also has a semantic relationship – an extending object provides a specialized use of the extended object.
      Extensions are used within the DDI-published packages to provide relationships between objects as they increase in
      complexity to meet increasingly complex functionality. Thus, a “simple” version of a questionnaire object might be extended
      into a more complex object, describing a more complex questionnaire.

   Framework
      The basic structure of something; a set of ideas or facts that provide support for something; a supporting structure;
      a structural frame (see http://www.merriam-webster.com/dictionary/framework).

   Functional View
      In DDI 4, a functional view identifies a set of objects that are needed to perform a specific task.
      It primarily consists of a set of references to specific versions of objects.
      Views are the method used to restrict the portions of the model that are used, and as such they function very much like
      DDI profiles in DDI 3.*.

   Identification
      In the DDI specifications, each object is uniquely identified.

   Instantiate
      To create an object of a specific class (see http://en.wiktionary.org/wiki/instantiate).

   Library
      The Object Library for DDI 4 encompasses the entire DDI 4.0 model, but without any specific schemas or vocabularies
      for Functional Views. Objects contain primitives and extended primitives and are the building blocks used to construct
      the Functional Views. Objects are organized into packages in the Library.

   Lifecycle
      The research data lifecycle is a set of processes that begins at study inception and progresses through data collection,
      data publication, data archiving, and beyond. DDI created a lifecycle model in 2004 to describe this flow
      (see http://www.ddialliance.org/system/files/Concept-Model-WD.pdf).

   Management package
      DDI 4 packages containing library constructs – primitives, extended primitives, objects, and functional views --
      which are organized thematically.

   Metadata
      Data about data.

   Modeling
      The representation, often mathematical, of a process, concept, or operation of a system, often implemented by a computer program.
      For DDI 4 development, we are using the Universal Modeling Language or UML to model the specification.

   Namespace
      A grouping of objects that allows for objects with the same name to be differentiated.
      The full name of an object is a combination of its namespace and its name within the namespace.

   Ontology
      A formal representation of knowledge (see http://en.wikipedia.org/wiki/Ontology_%28information_science%29).

   OWL
      Web Ontology Language is a semantic markup language for publishing and sharing ontologies on the World Wide Web (see http://www.w3.org/TR/owl-ref/).

   Platform
      A pre-existing environment in which to represent data and metadata (see http://en.wikipedia.org/wiki/Computing_platform).

   Primitive
      A basic type is a data type provided by a programming language as a basic building block.
      Most programming languages allow more complicated composite types to be recursively constructed starting from basic types.

   RDF
      The Resource Description Framework (RDF) is a family of
      World Wide Web Consortium (W3C) specifications[1] originally
      designed as a metadata data model. It has come to be used as a
      general method for conceptual description or modeling of
      information that is implemented in web resources, using a
      variety of syntax notations and data serialization formats.
      It is also used in knowledge management applications (see http://www.w3.org/RDF/).

   Serialization
      Transformation of some structure into a specific representation (see http://en.wikipedia.org/wiki/Serialization).

   Sprint
      An activity where a group of people comes together to work exclusively on some project.

   Study
      An activity producing data.

   UML
      The Unified Modeling Language (see http://www.uml.org/).

   UML Class Model
      A model describing objects and their relationships in the Unified Modeling Language (see http://www.uml.org/).

   UML Package
      A grouping of classes in the Library (see namespace).

   URI
      Uniform Resource Identifier,  a unique string of characters used to identify an object (see http://en.wikipedia.org/wiki/Uniform_resource_identifier).

   Versioning
      The assignment of some ordered attribute to objects, In the DDI model whenever a change is made to an approved object it must be
      given a new version. Objects with the same name and different versions are considered to be separate objects.

   Workflow
      A formalized set of processes carried out in a defined sequence (for more detail see http://en.wikipedia.org/wiki/Workflow).

   XMI
      An Object Management Group  XML  standard for  exchanging metadata information (see http://www.omg.org/spec/XMI/).

   xml
      Extensible Markup Language (XML) is a markup language that defines a
      set of rules for encoding documents in a format which is both
      human-readable and machine-readable.

   XSD
      An XML Schema definition (see http://www.w3.org/XML/Schema.html).