********************
Guide to editing
********************

Links
=====

to make an external link just type it http://www.ddialliance.org

For a link a text

.. code-block:: rst

   `Link text <http://example.com/>`_

To reference a section anywhere in the documentation e.g. :ref:`Question`

.. code-block:: rst

   :ref:`Question`

Strong
======

To mark something as **bold text**

.. code-block:: text

   **bold text**

Italic
======

To mark something as *italic text*

.. code-block:: text

   *italic text*

Code
====

To highlight some sourcecode

.. code-block:: rst

   .. code-block:: xml

      <some-xml></some-xml>

results in

.. code-block:: xml

   <some-xml></some-xml>

Headings
========

Headings are created by adding by adding underlining characters

.. code-block:: rst

   This is a  section heading
   ==========================

* # with overline, for parts
* \* with overline, for chapters
* =, for sections
* -, for subsections
* ^, for subsubsections
* ", for paragraphs

Images
======

To link a image

.. code-block:: rst

   .. image:: ddi-logo-mini.png

.. image:: ddi-logo-mini.png


Lists
=====
To do a list

.. code-block:: rst

   * This is a bulleted list.
   * This is item two

     * this is a sub item

   1. This is a numbered list.
   2. It has two items too.

Results in

* This is a bulleted list.
* This is item two

  * this is a sub item

1. This is a numbered list.
2. It has two items too.

Table
=====

To do a table

.. code-block:: rst

   =====  =====  =======
   Col 1  Col 2  Note
   =====  =====  =======
   1      False  cats
   2      False  dogs
   3      True   frame
   4      True   from
   =====  =====  =======

=====  =====  =======
Col 1  Col 2  Note
=====  =====  =======
1      False  cats
2      False  dogs
3      True   frame
4      True   from
=====  =====  =======

Glossary
========
To do a simple definition list

.. code-block:: rst

   .. glossary::

      cats
        miau is the sound of the cat.

      dogs
        voff is the sound of the dog.

Results in

.. glossary::

  cats
	miau is the sound of the cat.

  dogs
	voff is the sound of the dog.

For the documentation we should have a single glossary se :ref:`Glossary`

The we can make glossary links by writing

.. code-block:: rst

   :term:`xml`

and get :term:`xml`

Notes
=====

To add a note, warning etc.

.. code-block:: rst

   .. note::

      Above is the basics of reStructuredText.
      For a complete guide visit
      `Sphinx-doc <http://sphinx-doc.org/contents.html>`_ .

To get

.. note::

   Above is the basics of reStructuredText.
   For a complete guide visit
   `Sphinx-doc <http://sphinx-doc.org/contents.html>`_ .
