DDI Background
===============

DDI Is a metadata standard for describing data files and events sorounding the creation of the material.

Published versions of DDI

=======  ==============  ==================================================================================================
Version  Year published  Documentation link
=======  ==============  ==================================================================================================
2.1      2003            http://www.ddialliance.org/Specification/DDI-Codebook/2.1/DTD/Documentation/DDI2-1-tree.html
2.5      2012            http://www.ddialliance.org/Specification/DDI-Codebook/2.5/XMLSchema/field_level_documentation.html
3.0      2008            http://www.ddialliance.org/Specification/DDI-Lifecycle/3.0/XMLSchema/Documentation/
3.1      2009            http://www.ddialliance.org/Specification/DDI-Lifecycle/3.1/XMLSchema/FieldLevelDocumentation/
3.2      2012            http://www.ddialliance.org/Specification/DDI-Lifecycle/3.2/XMLSchema/FieldLevelDocumentation/
=======  ==============  ==================================================================================================

History
=======
Adding short text about the history of DDI.
