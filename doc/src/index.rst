==============================
DDI4 Documentation (dev build)
==============================
.. image:: ../_static/images/ddi-logo.*

.. warning::
  this is a development build, not a final product.

.. toctree::
   :caption: Table of contents
   :maxdepth: 2

   About/index.rst
   Introduction/index.rst
   usecases/index.rst
   userguides/index.rst
   Package/index.rst
   About/glossary.rst
   About/documentationSyntax.rst