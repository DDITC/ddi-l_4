.. _TemporalCoverage:

TemporalCoverage
****************
Describes the date or time period covered by the annotated object. Allows for the use of a specifying the type of coverage date as well as associated subjects or keywords.

Extends
=======
:ref:`AnnotatedIdentifiable`


Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "coverageDate", "ReferenceDate", "0..n"


coverageDate
############
A date referencing a specific aspect of temporal coverage. The date may be typed to reflect coverage date, collection date, referent date, etc. Subject and Keywords may be associated with the date to specify a specific set of topical information (i.e. Residence associated with a date 5 years prior to the collection date).




Graph
=====

.. graphviz:: /images/graph/Discovery/TemporalCoverage.dot