.. _TopicalCoverage:

TopicalCoverage
***************
Describes the topical coverage of the module using Subject and Keyword. Note that upper level modules should include all the members of lower level modules. Subjects are members of structured classification systems such as formal subject headings in libraries. Keywords may be structured (e.g. TheSoz thesauri) or unstructured and reflect the terminology found in the document and other related (broader or similar) terms.

Extends
=======
:ref:`AnnotatedIdentifiable`


Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "subject", "InternationalCodeValueType", "0..n"
   "keyword", "InternationalCodeValueType", "0..n"


subject
#######
A subject that describes the topical coverage of the content of the annotated object. Subjects are members of structured classification systems such as formal subject headings in libraries. Uses and InternationalCodeValue and may indicate the language of the code used.


keyword
#######
A keyword that describes the topical coverage of the content of the annotated object.  Keywords may be structured (e.g. TheSoz thesauri) or unstructured and reflect the terminology found in the document and other related (broader or similar) terms.  Uses and InternationalCodeValue and may indicate the language of the code used.




Graph
=====

.. graphviz:: /images/graph/Discovery/TopicalCoverage.dot