.. _SpatialCoverage:

SpatialCoverage
***************
A description of spatial coverage (geographic coverage) of the annotated object. Spatial coverage is described using a number of objects that support searching by a wide range of systems (geospatial coordinates, geographic classification systems, and general systems using dcterms:spatial.

Extends
=======
:ref:`AnnotatedIdentifiable`


Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "description", "Description", "0..1"
   "spatialAreaCode", "CodeValueType", "0..n"
   "spatialObject", "SpatialObject", "0..1"


description
###########
A textual description of the spatial coverage to support general searches.


spatialAreaCode
###############
Supports the use of a standardized code such as ISO 3166-1,  the Getty Thesaurus of Geographic Names, FIPS-5, etc.


spatialObject
#############
Indicates the most discrete spatial object type identified for a single case. Note that data can be collected at a geographic point (address) and reported as such in a protected file, and then aggregated to a polygon for a public file.




Graph
=====

.. graphviz:: /images/graph/Discovery/SpatialCoverage.dot