.. _BoundingBox:

BoundingBox
***********
A type of Spatial coverage describing a rectangular area within which the actual range of location fits. A BoundingBox can be described by 4 numbers, or two x,y coordinates - the maxima of the north, south, east, and west coordinates found in the area.

Extends
=======
:ref:`AnnotatedIdentifiable`


Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "eastLongitude", "xs:decimal", "1..1"
   "westLongitude", "xs:decimal", "1..1"
   "northLatitude", "xs:decimal", "1..1"
   "southLatitude", "xs:decimal", "1..1"


eastLongitude
#############
The easternmost coordinate expressed as a decimal between the values of -180 and 180 degrees


westLongitude
#############
The westernmost coordinate expressed as a decimal between the values of -180 and 180 degrees


northLatitude
#############
The northernmost coordinate expressed as a decimal between the values of -90 and 90 degrees.


southLatitude
#############
The southermost latitude expressed as a decimal between the values of -90 and 90 degrees




Graph
=====

.. graphviz:: /images/graph/Discovery/BoundingBox.dot