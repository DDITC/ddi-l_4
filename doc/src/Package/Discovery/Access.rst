.. _Access:

Access
******
Describes access to the annotated object. This item includes a confidentiality statement, descriptions of the access permissions required, restrictions to access, citation requirements, depositor requirements, conditions for access, a disclaimer, any time limits for access restrictions, and contact information regarding access.

Extends
=======
:ref:`AnnotatedIdentifiable`


Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "description", "StructuredString", "0..1"
   "confidentialityStatement", "StructuredString", "0..1"
   "accessPermission", "Form", "0..n"
   "restrictions", "StructuredString", "0..1"
   "citationRequirement", "StructuredString", "0..1"
   "depositRequirement", "StructuredString", "0..1"
   "accessConditions", "StructuredString", "0..1"
   "disclaimer", "StructuredString", "0..1"
   "contactAgent", "AgentAssociation", "0..n"
   "applyAccessTo", "InternationalIdentifier", "0..n"
   "validDates", "Date", "0..1"


description
###########
A description of the content and purpose of the access description. May be expressed in multiple languages and supports the use of structured content.


confidentialityStatement
########################
A statement regarding the confidentiality of the related data or metadata.


accessPermission
################
A link to a form used to provide access to the data or metadata including a statement of the purpose of the form.


restrictions
############
A statement regarding restrictions to access. May be expressed in multiple languages and supports the use of structured content.


citationRequirement
###################
A statement regarding the citation requirement. May be expressed in multiple languages and supports the use of structured content.


depositRequirement
##################
A statement regarding depositor requirements. May be expressed in multiple languages and supports the use of structured content.


accessConditions
################
A statement regarding conditions for access. May be expressed in multiple languages and supports the use of structured content.


disclaimer
##########
A disclaimer regarding the liability of the data producers or providers. May be expressed in multiple languages and supports the use of structured content.


contactAgent
############
The agent to contact regarding access including the role of the agent.


applyAccessTo
#############
Identification for an object covered by the access description. This may be any annotated object (collection, publication, identifiable object).


validDates
##########
The date range or start date of the access description.




Graph
=====

.. graphviz:: /images/graph/Discovery/Access.dot