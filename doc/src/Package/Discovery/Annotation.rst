.. _Annotation:

Annotation
**********
Provides annotation information on the object to support citation and crediting of the creator(s) of the object.

Extends
=======
:ref:`Identifiable`


Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "title", "InternationalString", "0..1"
   "subTitle", "InternationalString", "0..n"
   "alternateTitle", "InternationalString", "0..n"
   "creator", "AgentAssociation", "0..n"
   "publisher", "AgentAssociation", "0..n"
   "contributor", "AgentAssociation", "0..n"
   "date", "AnnotationDate", "0..n"
   "language", "CodeValueType", "0..n"
   "identifier", "InternationalIdentifier", "0..n"
   "copyright", "InternationalString", "0..n"
   "typeOfResource", "CodeValueType", "0..n"
   "informationSource", "InternationalString", "0..n"
   "versionIdentification", "xs:string", "0..1"
   "versionResponsibility", "AgentAssociation", "0..n"
   "abstract", "InternationalString", "0..1"
   "relatedResource", "ResourceIdentifier", "0..n"
   "provenance", "InternationalString", "0..n"
   "rights", "InternationalString", "0..n"
   "recordCreationDate", "xs:date", "0..1"
   "recordLastRevisionDate", "xs:date", "0..1"


title
#####
Full authoritative title. List any additional titles for this item as AlternativeTitle.


subTitle
########
Secondary or explanatory title.


alternateTitle
##############
An alternative title by which a data collection is commonly referred, or an abbreviation  for the title.


creator
#######
Person, corporate body, or agency responsible for the substantive and intellectual content of the described object.


publisher
#########
Person or organization responsible for making the resource available in its present form.


contributor
###########
The name of a contributing author or creator, who worked in support of the primary creator given above.


date
####
A date associated with the annotated object (not the coverage period). Use typeOfDate to specify the type of date such as Version, Publication, Submitted, Copyrighted, Accepted, etc.


language
########
Language of the intellectual content of the described object. Strongly recommend the use of language codes supported by xs:language which include the 2 and 3 character and extended structures defined by RFC4646 or its successors.


identifier
##########
An identifier or locator. Contains identifier and Managing agency (ISBN, ISSN, DOI, local archive). Indicates if it is a URI.


copyright
#########
The copyright statement.


typeOfResource
##############
Provide the type of the resource. This supports the use of a controlled vocabulary. It should be appropriate to the level of the annotation.


informationSource
#################
The name or identifier of source information for the annotated object.


versionIdentification
#####################
Means of identifying the current version of the annotated object.


versionResponsibility
#####################
The agent responsible for the version. May have an associated role.


abstract
########
An a abstract (description) of the annotated object.


relatedResource
###############
Provide the identifier, managing agency, and type of resource related to this object.


provenance
##########
A statement of any changes in ownership and custody of the resource since its creation that are significant for its authenticity, integrity, and interpretation.


rights
######
Information about rights held in and over the resource. Typically, rights information includes a statement about various property rights associated with the resource, including intellectual property rights.


recordCreationDate
##################
Date the record was created


recordLastRevisionDate
######################
Date the record was last revised




Graph
=====

.. graphviz:: /images/graph/Discovery/Annotation.dot