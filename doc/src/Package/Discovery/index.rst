*********
Discovery
*********
Contains objects used to provide annotation and coverage information for DDI publications (views, object sets, data files, etc.)
Contents 

.. toctree::
   :maxdepth: 2

   Access
   Annotation
   BoundingBox
   Coverage
   SpatialCoverage
   TemporalCoverage
   TopicalCoverage
