.. _Coverage:

Coverage
********
Coverage information for an annotated object. Includes coverage information for temporal, topical, and spatial coverage. 

Extends
=======
:ref:`AnnotatedIdentifiable`


Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "description", "Description", "0..n"


description
###########
A generic description including temporal, topical, and spatial coverage that is the equivalent of dc:coverage (the refinement base of dcterms:spatial and dcterms:temporal. Use specific coverage content for detailed information.




Graph
=====

.. graphviz:: /images/graph/Discovery/Coverage.dot