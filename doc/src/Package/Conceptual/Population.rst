.. _Population:

Population
**********
Set of specific units (people, entities, objects, events), usually in a given time and geography.

Extends
=======
:ref:`Universe`



Graph
=====

.. graphviz:: /images/graph/Conceptual/Population.dot