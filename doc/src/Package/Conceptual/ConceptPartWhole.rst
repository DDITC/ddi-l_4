.. _ConceptPartWhole:

ConceptPartWhole
****************
Part-whole specialization of OrderRelation between Concepts within a ConceptSystem.


Extends
=======
:ref:`OrderRelation`



Graph
=====

.. graphviz:: /images/graph/Conceptual/ConceptPartWhole.dot