.. _EnumeratedConceptualDomain:

EnumeratedConceptualDomain
**************************
A Conceptual Domain expressed as a list of Categories.

Extends
=======
:ref:`ConceptualDomain`



Graph
=====

.. graphviz:: /images/graph/Conceptual/EnumeratedConceptualDomain.dot