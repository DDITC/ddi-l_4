.. _InstanceVariable:

InstanceVariable
****************
The use of a Represented Variable within a Data Set.  

Extends
=======
:ref:`RepresentedVariable`


Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "variableRole", "StructuredString", "0..1"


variableRole
############
An Instance Variable can take different roles, e.g. Identifier, Measure and Attribute. Note that DataStructure takes care of the ordering of Identifiers.




Graph
=====

.. graphviz:: /images/graph/Conceptual/InstanceVariable.dot