.. _ConceptualDomain:

ConceptualDomain
****************
Set of valid Concepts. The Concepts can be described by either enumeration or by an expression.

Extends
=======
:ref:`AnnotatedIdentifiable`


Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "label", "Label", "0..n"
   "definition", "StructuredString", "0..1"
   "description", "StructuredString", "0..1"


label
#####
A display label for the Conceptual Domain. May be expressed in multiple languages. Repeat for labels with different content, for example, labels with differing length limitations.


definition
##########
A description of the content and purpose of the Conceptual Domain. May be expressed in multiple languages and supports the use of structured content.


description
###########
A description of the purpose or use of a concept. May be expressed in multiple languages and supports the use of structured content.




Graph
=====

.. graphviz:: /images/graph/Conceptual/ConceptualDomain.dot