.. _ConceptualVariable:

ConceptualVariable
******************
The use of a Concept as a characteristic of a Universe intended to be measured [GSIM 1.1]

Extends
=======
:ref:`Concept`



Graph
=====

.. graphviz:: /images/graph/Conceptual/ConceptualVariable.dot