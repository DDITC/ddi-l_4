.. _Universe:

Universe
********
A defined class of people, entities, events, or objects, with no specification of time and geography, contextualizing a Unit Type

Extends
=======
:ref:`UnitType`


Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "isInclusive", "xs:boolean", "0..1"


isInclusive
###########
The default value is "true". The description statement of a universe is generally stated in inclusive terms such as "All persons with university degree". Occasionally a universe is defined by what it excludes, i.e., "All persons except those with university degree". In this case the value would be changed to "false".




Graph
=====

.. graphviz:: /images/graph/Conceptual/Universe.dot