.. _RepresentedVariable:

RepresentedVariable
*******************
A combination of a characteristic of a universe to be measured and how that measure will be represented.


Extends
=======
:ref:`ConceptualVariable`



Graph
=====

.. graphviz:: /images/graph/Conceptual/RepresentedVariable.dot