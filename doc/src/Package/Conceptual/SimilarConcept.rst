.. _SimilarConcept:

SimilarConcept
**************
A reference to a concept with similar meaning and a description of their differences. The similar concept structure allows specification of similar concepts to address cases where confusion may affect the appropriate use of the concept.

Extends
=======
:ref:`MemberCorrespondence`



Graph
=====

.. graphviz:: /images/graph/Conceptual/SimilarConcept.dot