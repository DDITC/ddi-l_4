.. _UnitType:

UnitType
********
A Unit Type is a class of objects of interest.

Extends
=======
:ref:`Concept`



Graph
=====

.. graphviz:: /images/graph/Conceptual/UnitType.dot