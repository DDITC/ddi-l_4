.. _DescribedConceptualDomain:

DescribedConceptualDomain
*************************
A Conceptual Domain defined by an expression.

Extends
=======
:ref:`ConceptualDomain`



Graph
=====

.. graphviz:: /images/graph/Conceptual/DescribedConceptualDomain.dot