**********
Conceptual
**********
Conceptual covers all of the basic components of ISO/IEC 11179 as captured and represented in the GSIM Concepts Group http://www1.unece.org/stat/platform/display/GSIMclick/Concepts+Group 
The Conceptual package will review all parts of the GSIM Concepts Group content and determine if and where adjustments need to be made to interact well with the overall mandate of DDI regarding support for work outside of the GSIM sphere and required levels of abstraction. 

Contents 

.. toctree::
   :maxdepth: 2

   Category
   Concept
   ConceptParentChild
   ConceptPartWhole
   ConceptSystem
   ConceptSystemCorrespondence
   ConceptualDomain
   ConceptualVariable
   DescribedConceptualDomain
   EnumeratedConceptualDomain
   InstanceVariable
   Population
   RepresentedVariable
   SimilarConcept
   Unit
   UnitType
   Universe
