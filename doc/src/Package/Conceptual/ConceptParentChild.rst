.. _ConceptParentChild:

ConceptParentChild
******************
Parent-child specialization of OrderRelation between Concepts within a ConceptSystem.

Extends
=======
:ref:`OrderRelation`



Graph
=====

.. graphviz:: /images/graph/Conceptual/ConceptParentChild.dot