.. _Unit:

Unit
****
The object of interest in a process step related to the collection or use of observational data.

Extends
=======
:ref:`AnnotatedIdentifiable`


Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "label", "Label", "0..n"
   "definition", "StructuredString", "0..1"


label
#####
A display label for the Unit. May be expressed in multiple languages. Repeat for labels with different content, for example, labels with differing length limitations.


definition
##########
A description of the content and purpose of the Unit. May be expressed in multiple languages and supports the use of structured content.




Graph
=====

.. graphviz:: /images/graph/Conceptual/Unit.dot