.. _Concept:

Concept
*******
Unit of thought differentiated by characteristics [GSIM 1.1]

Extends
=======
:ref:`Member`



Graph
=====

.. graphviz:: /images/graph/Conceptual/Concept.dot