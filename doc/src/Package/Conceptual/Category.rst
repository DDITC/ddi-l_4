.. _Category:

Category
********
A Concept whose role is to define and measure a characteristic.

Extends
=======
:ref:`Concept`



Graph
=====

.. graphviz:: /images/graph/Conceptual/Category.dot