.. _ConceptSystem:

ConceptSystem
*************
A set of Concepts structured by the relations among them. [GSIM 1.1]

Extends
=======
:ref:`Collection`



Graph
=====

.. graphviz:: /images/graph/Conceptual/ConceptSystem.dot