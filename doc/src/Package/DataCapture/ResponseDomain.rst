.. _ResponseDomain:

ResponseDomain
**************
The possible list of values that are allowed by a Capture.

Extends
=======
:ref:`AnnotatedIdentifiable`



Graph
=====

.. graphviz:: /images/graph/DataCapture/ResponseDomain.dot