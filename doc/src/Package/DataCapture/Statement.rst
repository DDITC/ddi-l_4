.. _Statement:

Statement
*********
A Statement is human readable text or referred material.

Extends
=======
:ref:`InstrumentComponent`


Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "text", "StructuredString", "1..1"


text
####
Structured human-readable text




Graph
=====

.. graphviz:: /images/graph/DataCapture/Statement.dot