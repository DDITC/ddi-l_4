.. _ImplementedInstrument:

ImplementedInstrument
*********************
ImplementedInstruments are mode and/or unit specific.

Extends
=======
:ref:`AnnotatedIdentifiable`



Graph
=====

.. graphviz:: /images/graph/DataCapture/ImplementedInstrument.dot