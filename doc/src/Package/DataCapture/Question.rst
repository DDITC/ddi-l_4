.. _Question:

Question
********
A query of a human subject

Extends
=======
:ref:`Capture`


Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "text", "Text", "1..1"
   "language", "xs:language", "1..1"


text
####
Literal question text


language
########





Graph
=====

.. graphviz:: /images/graph/DataCapture/Question.dot