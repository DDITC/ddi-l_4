.. _Observation:

Observation
***********
The result of applying a particular Capture in an Instrument to some experimental Unit




Graph
=====

.. graphviz:: /images/graph/DataCapture/Observation.dot