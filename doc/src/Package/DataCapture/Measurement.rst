.. _Measurement:

Measurement
***********
Any data capture method other than Question.

Extends
=======
:ref:`Capture`



Graph
=====

.. graphviz:: /images/graph/DataCapture/Measurement.dot