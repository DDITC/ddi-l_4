.. _ExternalAid:

ExternalAid
***********
Any external stimulus material used in an instrument that aids or facilitates data capture, or that is presented to a respondent and about which measurements are made. 

Extends
=======
:ref:`OtherMaterial`


Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "stimulusType", "CodeValueType", "0..1"


stimulusType
############





Graph
=====

.. graphviz:: /images/graph/DataCapture/ExternalAid.dot