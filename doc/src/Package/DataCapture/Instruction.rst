.. _Instruction:

Instruction
***********
Provides the content and description of data capture instructions. Contains the "how to" information for administering an instrument.

Extends
=======
:ref:`InstrumentComponent`


Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "associatedImage", "StructuredString", "0..1"
   "instructionText", "DynamicText", "0..n"


associatedImage
###############
An image associated with the Instruction, located at the provided URN or URL.


instructionText
###############
The content of the Instruction text provided using DynamicText. Note that when using Dynamic Text, the full InstructionText must be repeated for multi-language versions of the content. The InstructionText may also be repeated to provide a dynamic and plain text version of the instruction. This allows for accurate rendering of the instruction in a non-dynamic environment like print.




Graph
=====

.. graphviz:: /images/graph/DataCapture/Instruction.dot