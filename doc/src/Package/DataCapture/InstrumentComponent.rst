.. _InstrumentComponent:

InstrumentComponent
*******************
InstrumentComponent is an abstract object which extends ProcessStep. The purpose of InstrumentComponent is to provide a common parent for Capture (e.g., Question, Measure), Statement, and Instructions.

Extends
=======
:ref:`ProcessStep`



Graph
=====

.. graphviz:: /images/graph/DataCapture/InstrumentComponent.dot