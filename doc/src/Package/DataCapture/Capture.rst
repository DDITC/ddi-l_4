.. _Capture:

Capture
*******
A Capture is an abstract object. Concrete objects that extend Capture describe the means of obtaining research data.

Extends
=======
:ref:`InstrumentComponent`



Graph
=====

.. graphviz:: /images/graph/DataCapture/Capture.dot