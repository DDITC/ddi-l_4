.. _ConceptualInstrument:

ConceptualInstrument
********************
Design plan for creating a data capture tool. From GSIM: The complete questionnaire design with a relationship to the top level questionnaire component (control construct). 

Extends
=======
:ref:`AnnotatedIdentifiable`



Graph
=====

.. graphviz:: /images/graph/DataCapture/ConceptualInstrument.dot