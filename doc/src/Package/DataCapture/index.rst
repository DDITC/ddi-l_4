***********
DataCapture
***********
The DataCapture package contains objects used to describe the process of collecting, acquiring, or capturing data from various sources. Sources could include surveys, databases, registries, administrative data, bio-medical devices, environmental sensors, or any other source or instrument.

Note: this package was previously referred to as SimpleInstrument.
Contents 

.. toctree::
   :maxdepth: 2

   Capture
   ConceptualInstrument
   ExternalAid
   ImplementedInstrument
   Instruction
   InstrumentComponent
   Measurement
   Observation
   Question
   ResponseDomain
   Statement
