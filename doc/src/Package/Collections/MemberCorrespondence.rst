.. _MemberCorrespondence:

MemberCorrespondence
********************
Generic (untyped) relationship between members of collections.

Extends
=======
:ref:`AnnotatedIdentifiable`


Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "type", "CorrespondenceType", "0..1"
   "label", "Label", "0..n"
   "definition", "StructuredString", "0..1"
   "description", "StructuredString", "0..1"


type
####
Type of correspondence in terms of commonalities and differences between two members.


label
#####
A display label for the MemberCorrespondence. May be expressed in multiple languages. Repeat for labels with different content, for example, labels with differing length limitations.


definition
##########
A definition of the MemberCorrespondence. May be expressed in multiple languages and supports the use of structured content.


description
###########
A description of the purpose or use of the MemberCorrespondence. May be expressed in multiple languages and supports the use of structured content.




Graph
=====

.. graphviz:: /images/graph/Collections/MemberCorrespondence.dot