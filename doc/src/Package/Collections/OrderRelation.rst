.. _OrderRelation:

OrderRelation
*************
Binary relation over members in a collection (set or bag) that is always reflexive, antisymmetric, and transitive. It can also be either total or partial. It must contain like items.

Extends
=======
:ref:`AnnotatedIdentifiable`


Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "type", "OrderRelationshipType", "0..1"
   "criteria", "StructuredString", "0..1"
   "isRegularHierarchy", "xs:boolean", "0..1"
   "label", "Label", "0..n"
   "definition", "StructuredString", "0..1"
   "description", "StructuredString", "0..1"


type
####
Whether the order relation is total or partial.


criteria
########
Intensional definition of the order criteria (e.g. alphabetical, numerical, increasing, decreasing, etc.)


isRegularHierarchy
##################
Indicates whether the tree defined by the order relation is regular or not. i.e., all leaves are at the same level..


label
#####
A display label for the OrderRelation. May be expressed in multiple languages. Repeat for labels with different content, for example, labels with differing length limitations.


definition
##########
A definition of the OrderRelation. May be expressed in multiple languages and supports the use of structured content.


description
###########
A description of the purpose or use of the OrderRelation. May be expressed in multiple languages and supports the use of structured content.




Graph
=====

.. graphviz:: /images/graph/Collections/OrderRelation.dot