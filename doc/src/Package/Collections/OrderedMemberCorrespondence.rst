.. _OrderedMemberCorrespondence:

OrderedMemberCorrespondence
***************************
Ordered relationship between members of collections.

Extends
=======
:ref:`MemberCorrespondence`



Graph
=====

.. graphviz:: /images/graph/Collections/OrderedMemberCorrespondence.dot