***********
Collections
***********
Generic collection structure to support managed and unmanaged collections containing both unique and non-unique members. It also supports the definition of correspondences, unordered and ordered, between collections and members. Such a generic structure can be used to model different types of groupings, from simple unordered sets to all sorts of hierarchies, nesting and ordered sets/bags. In addition, they can be extended with richer semantics (e.g. generic, partitive, and instance, among others).
Contents 

.. toctree::
   :maxdepth: 2

   Collection
   CollectionCorrespondence
   Member
   MemberCorrespondence
   OrderRelation
   OrderedCollectionCorrespondence
   OrderedMemberCorrespondence
