.. _Collection:

Collection
**********
Collection container (set or bag). It could have an optional order relation (total or partial) associated to it to model linear order, hierarchies and nesting. A Collection is also a subtype of Member to allow for nested collections.

Extends
=======
:ref:`Member`


Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "type", "CollectionType", "0..1"


type
####
Whether the collection is a bag or a set.




Graph
=====

.. graphviz:: /images/graph/Collections/Collection.dot