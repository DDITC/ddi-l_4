.. _OrderedCollectionCorrespondence:

OrderedCollectionCorrespondence
*******************************
Generic (untyped) ordered relationship between collections.

Extends
=======
:ref:`CollectionCorrespondence`



Graph
=====

.. graphviz:: /images/graph/Collections/OrderedCollectionCorrespondence.dot