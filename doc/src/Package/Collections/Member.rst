.. _Member:

Member
******
Generic class representing members of a collection. 


Extends
=======
:ref:`AnnotatedIdentifiable`


Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "label", "Label", "0..n"
   "definition", "StructuredString", "0..1"
   "description", "StructuredString", "0..1"


label
#####
A display label for the Member. May be expressed in multiple languages. Repeat for labels with different content, for example, labels with differing length limitations.


definition
##########
A definition of the Member. May be expressed in multiple languages and supports the use of structured content.


description
###########
A description of the purpose or use of the Member. May be expressed in multiple languages and supports the use of structured content.




Graph
=====

.. graphviz:: /images/graph/Collections/Member.dot