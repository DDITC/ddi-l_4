.. _NodeSetPartWhole:

NodeSetPartWhole
****************
Part-whole specialization of OrderRelation between NodeSets.
The inherited type property is set to "partial" to specify that the part-whole relationships among NodeSets define a tree structure.

Extends
=======
:ref:`OrderRelation`



Graph
=====

.. graphviz:: /images/graph/Representations/NodeSetPartWhole.dot