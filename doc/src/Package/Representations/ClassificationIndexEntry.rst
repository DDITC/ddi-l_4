.. _ClassificationIndexEntry:

ClassificationIndexEntry
************************
A Classification Index Entry is a word or a short text (e.g. the name of a locality, an economic activity or an occupational title) describing a type of object/unit or object property to which a Classification Item applies, together with the code of the corresponding Classification Item. Each Classification Index Entry typically refers to one item of the Statistical Classification. Although a Classification Index Entry may be associated with a Classification Item at any Level of a Statistical Classification, Classification Index Entries are normally associated with items at the lowest Level.

Extends
=======
:ref:`Member`


Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "text", "InternationalString", "1..n"
   "validfrom", "Date", "0..1"
   "validto", "Date", "0..1"
   "codingInstructions", "InternationalString", "0..n"


text
####
Text describing the type of object/unit or object property.


validfrom
#########
Date from which the Classification Index Entry became valid. The date must be defined if the Classification Index Entry belongs to a floating Classification Index.


validto
#######
Date at which the Classification Index Entry became invalid. The date must be defined if the Classification Index Entry belongs to a floating Classification Index and is no longer valid.


codingInstructions
##################
Additional information which drives the coding process. Required when coding is dependent upon one or many other factors.




Graph
=====

.. graphviz:: /images/graph/Representations/ClassificationIndexEntry.dot