.. _ValueDomain:

ValueDomain
***********
The permitted range of values for a characteristic of a variable. [GSIM 1.1]

Extends
=======
:ref:`AnnotatedIdentifiable`


Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "unitOfMeasurement", "xs:string", "0..1"
   "label", "Label", "0..n"
   "definition", "StructuredString", "0..1"
   "description", "StructuredString", "0..1"


unitOfMeasurement
#################
The unit in which the data values are measured (kg, pound, euro).


label
#####
A display label for the object. May be expressed in multiple languages. Repeat for labels with different content, for example, labels with differing length limitations.


definition
##########
A definition of the object. May be expressed in multiple languages and supports the use of structured content.


description
###########
A description of the content and purpose of the object. May be expressed in multiple languages and supports the use of structured content.




Graph
=====

.. graphviz:: /images/graph/Representations/ValueDomain.dot