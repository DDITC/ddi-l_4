.. _Designation:

Designation
***********
The name given to an object for identification.

Extends
=======
:ref:`AnnotatedIdentifiable`


Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "label", "Label", "0..n"
   "description", "StructuredString", "0..1"


label
#####
A display label for the Designation. May be expressed in multiple languages. Repeat for labels with different content, for example, labels with differing length limitations.


description
###########
A description of the purpose or use of the Designation. May be expressed in multiple languages and supports the use of structured content.




Graph
=====

.. graphviz:: /images/graph/Representations/Designation.dot