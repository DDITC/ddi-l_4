.. _StatisticalClassification:

StatisticalClassification
*************************
A Statistical Classification is a set of Categories which may be assigned to one or more variables registered in statistical surveys or administrative files, and used in the production and dissemination of statistics. The Categories at each Level of the classification structure must be mutually exclusive and jointly exhaustive of all objects/units in the population of interest. (Source: GSIM StatisticalClassification)

Extends
=======
:ref:`NodeSet`


Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "introduction", "StructuredString", "0..1"
   "releaseDate", "Date", "0..1"
   "terminationDate", "Date", "0..1"
   "validDate", "Date", "0..1"
   "isCurrent", "xs:boolean", "0..1"
   "isFloating", "xs:boolean", "0..1"
   "changeFromBase", "StructuredString", "0..1"
   "purposeOfVariant", "StructuredString", "0..1"
   "copyright", "String", "0..n"
   "updateChanges", "StructuredString", "0..n"
   "availableLanguage", "xs:language", "0..n"


introduction
############
The introduction provides a detailed description of the Statistical Classification, the background for its creation, the classification variable and objects/units classified, classification rules etc. (Source: GSIM StatisticalClassification


releaseDate
###########
Date the Statistical Classification was released


terminationDate
###############
Date on which the Statistical Classification was superseded by a successor version or otherwise ceased to be valid. (Source: GSIM Statistical Classification)


validDate
#########
The date the statistical classification enters production use.


isCurrent
#########
Indicates if the Statistical Classification is currently valid.


isFloating
##########
Indicates if the Statistical Classification is a floating classification. In a floating statistical classification, a validity period should be defined for all Classification Items which will allow the display of the item structure and content at different points of time. (Source: GSIM StatisticalClassification/Floating


changeFromBase
##############
Describes the relationship between the variant and its base Statistical Classification, including regroupings, aggregations added and extensions. (Source: GSIM StatisticalClassification/Changes from base Statistical Classification)


purposeOfVariant
################
If the Statistical Classification is a variant, notes the specific purpose for which it was developed. (Source: GSIM StatisticalClassification/Purpose of variant)


copyright
#########
Copyright of the statistical classification.


updateChanges
#############
Summary description of changes which have occurred since the most recent classification version or classification update came into force.


availableLanguage
#################
A list of languages in which the Statistical Classification is available. Repeat for each langauge.




Graph
=====

.. graphviz:: /images/graph/Representations/StatisticalClassification.dot