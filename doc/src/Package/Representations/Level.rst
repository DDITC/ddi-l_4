.. _Level:

Level
*****
The Level describes the nesting structure of a hierarchical collection.



Extends
=======
:ref:`Collection`



Graph
=====

.. graphviz:: /images/graph/Representations/Level.dot