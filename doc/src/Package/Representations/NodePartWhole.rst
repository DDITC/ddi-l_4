.. _NodePartWhole:

NodePartWhole
*************
Part-whole specialization of OrderRelation between Nodes within a NodeSet.
The inherited type property is set to "partial" to specify that the part-whole relationships among Nodes define a tree structure.


Extends
=======
:ref:`OrderRelation`



Graph
=====

.. graphviz:: /images/graph/Representations/NodePartWhole.dot