.. _LevelParentChild:

LevelParentChild
****************
Parent-child specialization of OrderRelation between Levels within a NodeSet.
The inherited type property is set to "total" to specify that the parent-child relationships among Levels in any given NodeSet define a linear sequence.

Extends
=======
:ref:`OrderRelation`



Graph
=====

.. graphviz:: /images/graph/Representations/LevelParentChild.dot