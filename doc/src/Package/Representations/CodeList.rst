.. _CodeList:

CodeList
********
A list of Codes and associated Categories. May be flat or hierarchical. 

Extends
=======
:ref:`NodeSet`



Graph
=====

.. graphviz:: /images/graph/Representations/CodeList.dot