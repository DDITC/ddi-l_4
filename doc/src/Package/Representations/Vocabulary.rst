.. _Vocabulary:

Vocabulary
**********
A vocabulary is an established list of standardized terminology for use in indexing and retrieval of information.

Extends
=======
:ref:`ConceptSystem`


Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "abbreviation", "InternationalString", "0..n"
   "location", "URI", "0..1"
   "comments", "StructuredString", "0..n"


abbreviation
############
Abbreviation of vocabulary title.


location
########
Location of external resource providing information about the vocabulary.


comments
########
Information for the user regarding the reasons for use of the vocabulary and appropriate usage constraints.




Graph
=====

.. graphviz:: /images/graph/Representations/Vocabulary.dot