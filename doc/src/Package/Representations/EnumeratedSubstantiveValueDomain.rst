.. _EnumeratedSubstantiveValueDomain:

EnumeratedSubstantiveValueDomain
********************************
An enumerated value domain whose values are associated with the scientific questions of interest in a study.

Extends
=======
:ref:`EnumeratedValueDomain`



Graph
=====

.. graphviz:: /images/graph/Representations/EnumeratedSubstantiveValueDomain.dot