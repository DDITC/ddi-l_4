.. _ClassificationSeries:

ClassificationSeries
********************
A Classification Series is an ensemble of one or more Statistical Classifications, based on the same concept, and related to each other as versions or updates. Typically, these Statistical Classifications have the same name (for example, ISIC or ISCO).

Extends
=======
:ref:`Collection`


Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "context", "StructuredString", "0..1"
   "objectsOrUnitsClassified", "StructuredString", "1..1"
   "subjectAreas", "StructuredString", "1..1"
   "owners", "String", "0..1"
   "keywords", "StructuredString", "0..n"


context
#######
ClassificationSeries can be designed in a specific context.


objectsOrUnitsClassified
########################
A ClassificationSeries is designed to classify a specific type of object/unit according to a specific attribute.


subjectAreas
############
Areas of statistics in which the ClassificationSeries is implemented.


owners
######
The statistical office or other authority, which created and maintains the StatisticalClassification (s) related to the ClassificationSeries. A ClassificationSeries may have several owners.


keywords
########
A ClassificationSeries can be associated with one or a number of keywords.




Graph
=====

.. graphviz:: /images/graph/Representations/ClassificationSeries.dot