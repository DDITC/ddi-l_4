.. _Sign:

Sign
****
Something that suggests the presence or existence of a fact, condition, or quality.

Extends
=======
:ref:`AnnotatedIdentifiable`


Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "value", "StructuredString", "1..1"
   "label", "Label", "0..n"
   "description", "StructuredString", "0..1"


value
#####
The text representation


label
#####
A display label for the object. May be expressed in multiple languages. Repeat for labels with different content, for example, labels with differing length limitations.


description
###########
A description of the content and purpose of the object. May be expressed in multiple languages and supports the use of structured content.




Graph
=====

.. graphviz:: /images/graph/Representations/Sign.dot