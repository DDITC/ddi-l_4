.. _EnumeratedSentinelValueDomain:

EnumeratedSentinelValueDomain
*****************************
An enumerated value domain whose values are used only for the processing of data after capture but before dissemination.

Extends
=======
:ref:`EnumeratedValueDomain`



Graph
=====

.. graphviz:: /images/graph/Representations/EnumeratedSentinelValueDomain.dot