.. _Code:

Code
****
A Designation for a Category.

Extends
=======
:ref:`Designation`


Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "value", "Value", "1..1"


value
#####
Specified value of the code




Graph
=====

.. graphviz:: /images/graph/Representations/Code.dot