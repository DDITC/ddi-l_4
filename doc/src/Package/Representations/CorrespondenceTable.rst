.. _CorrespondenceTable:

CorrespondenceTable
*******************
A Correspondence Table expresses a relationship between two NodeSets.

Extends
=======
:ref:`OrderedCollectionCorrespondence`


Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "owners", "String", "0..1"
   "maintenanceUnit", "String", "0..1"
   "contactPersons", "String", "0..n"
   "publications", "StructuredString", "0..n"
   "effectivePeriod", "Date", "0..1"


owners
######
The statistical office, other authority or section that created and maintains the Correspondence Table. A Correspondence Table may have several owners.


maintenanceUnit
###############
The unit or group of persons who are responsible for the Correspondence Table, i.e. for maintaining and updating it.


contactPersons
##############
The person(s) who may be contacted for additional information about the Correspondence Table.


publications
############
A list of the publications in which the Correspondence Table has been published.


effectivePeriod
###############
Effective period of validity of the CorrespondenceTable. The correspondence table expresses the relationships between the two NodeSets as they existed on the period specified in the table.




Graph
=====

.. graphviz:: /images/graph/Representations/CorrespondenceTable.dot