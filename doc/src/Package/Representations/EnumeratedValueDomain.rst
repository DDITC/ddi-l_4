.. _EnumeratedValueDomain:

EnumeratedValueDomain
*********************
A Value domain expressed as a list of categories and associated codes.

Extends
=======
:ref:`ValueDomain`



Graph
=====

.. graphviz:: /images/graph/Representations/EnumeratedValueDomain.dot