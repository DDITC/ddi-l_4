.. _CategorySet:

CategorySet
***********
A Category Set is a type of Node Set which groups Categories.


Extends
=======
:ref:`NodeSet`



Graph
=====

.. graphviz:: /images/graph/Representations/CategorySet.dot