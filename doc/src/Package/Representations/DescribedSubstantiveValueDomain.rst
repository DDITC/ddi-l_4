.. _DescribedSubstantiveValueDomain:

DescribedSubstantiveValueDomain
*******************************
A described value domain whose values are associated with the scientific questions of interest in a study.

Extends
=======
:ref:`DescribedValueDomain`



Graph
=====

.. graphviz:: /images/graph/Representations/DescribedSubstantiveValueDomain.dot