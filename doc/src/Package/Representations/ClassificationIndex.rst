.. _ClassificationIndex:

ClassificationIndex
*******************
A Classification Index is an ordered list (alphabetical, in code order etc) of Classification Index Entries. A Classification Index can relate to one particular or to several Statistical Classifications. [GSIM Statistical Classification Model]

Extends
=======
:ref:`Collection`


Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "releaseDate", "Date", "0..1"
   "maintenanceUnit", "InternationalString", "0..1"
   "contactPersons", "InternationalString", "0..1"
   "publications", "InternationalString", "0..n"
   "languages", "InternationalString", "0..n"
   "corrections", "InternationalString", "0..n"
   "codingInstructions", "InternationalString", "0..n"


releaseDate
###########
Date when the current version of the Classification Index was released.


maintenanceUnit
###############
The unit or group of persons within the organisation responsible for the Classification Index, i.e. for adding, changing or deleting Classification Index Entries.


contactPersons
##############
Person(s) who may be contacted for additional information about the Classification Index.


publications
############
A list of the publications in which the Classification Index has been published.


languages
#########
A Classification Index can exist in several languages. Indicates the languages available. If a Classification Index exists in several languages, the number of entries in each language may be different, as the number of terms describing the same phenomenon can change from one language to another. However, the same phenomena should be described in each language.


corrections
###########
Verbal summary description of corrections, which have occurred within the Classification Index. Corrections include changing the item code associated with an Classification Index Entry.


codingInstructions
##################
Additional information which drives the coding process for all entries in a Classification Index.




Graph
=====

.. graphviz:: /images/graph/Representations/ClassificationIndex.dot