.. _DataType:

DataType
********
Set of distinct values, characterized by properties of those values, and by operations on those values.
(From ISO/IEC 11404 - General purpose datatypes) 

Extends
=======
:ref:`AnnotatedIdentifiable`


Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "scheme", "InternationalString", "1..1"


scheme
######
ISO 11404, Excel, SAS, R, etc.




Graph
=====

.. graphviz:: /images/graph/Representations/DataType.dot