.. _ClassificationItem:

ClassificationItem
******************
A Classification Item represents a Category at a certain Level within a Statistical Classification.


Extends
=======
:ref:`Node`


Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "isValid", "xs:boolean", "0..1"
   "isGenerated", "xs:boolean", "0..1"
   "explanatoryNotes", "StructuredString", "0..n"
   "futureNotes", "InternationalString", "0..n"
   "changeLog", "InternationalString", "0..1"
   "changeFromPreviousVersion", "InternationalString", "0..1"
   "validDate", "Date", "0..1"
   "officialName", "Name", "1..1"


isValid
#######
Indicates whether or not the item is currently valid. If updates are allowed in the Statistical Classification, an item may be restricted in its validity, i.e. it may become valid or invalid after the Statistical Classification has been released.


isGenerated
###########
Indicates whether or not the item has been generated to make the level to which it belongs complete


explanatoryNotes
################
A Classification Item may be associated with explanatory notes, which further describe and clarify the contents of the Category. Explanatory notes consist of: General note: Contains either additional information about the Category, or a general description of the Category, which is not structured according to the "includes", "includes also", "excludes" pattern. Includes: Specifies the contents of the Category. Includes also: A list of borderline cases, which belong to the described Category. Excludes: A list of borderline cases, which do not belong to the described Category. Excluded cases may contain a reference to the Classification Items to which the excluded cases belong.


futureNotes
###########
The future events describe a change (or a number of changes) related to an invalid item. These changes may e.g. have turned the now invalid item into one or several successor items. This allows the possibility to follow successors of the item in the future.


changeLog
#########
Describes the changes, which the item has been subject to during the life time of the actual Statistical Classification.


changeFromPreviousVersion
#########################
Describes the changes, which the item has been subject to from the previous version to the actual Statistical Classification


validDate
#########
Dates for which the classification is valid. Date from which the item became valid. The date must be defined if the item belongs to a floating Statistical classification. Date at which the item became invalid. The date must be defined if the item belongs to a floating Statistical classification and is no longer valid


officialName
############
A Classification Item has a name as provided by the owner or maintenance unit. The name describes the content of the category. The name is unique within the Statistical Classification to which the item belongs, except for categories that are identical at more than one level in a hierarchical classification




Graph
=====

.. graphviz:: /images/graph/Representations/ClassificationItem.dot