.. _ClassificationFamily:

ClassificationFamily
********************
A Classification Family is a group of Classification Series related from a particular point of view. The Classification Family is related by being based on a common Concept (e.g. economic activity).[GSIM1.1]

Extends
=======
:ref:`Collection`



Graph
=====

.. graphviz:: /images/graph/Representations/ClassificationFamily.dot