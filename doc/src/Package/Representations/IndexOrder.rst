.. _IndexOrder:

IndexOrder
**********
Indexing order, defined either by predecessor-successor pairs or by a criteria (e.g. alphabetical, in code order, etc.)

Extends
=======
:ref:`OrderRelation`



Graph
=====

.. graphviz:: /images/graph/Representations/IndexOrder.dot