.. _NodeSet:

NodeSet
*******
A NodeSet is a set of Nodes, which could be organized into a hierarchy of Levels.

Extends
=======
:ref:`Collection`



Graph
=====

.. graphviz:: /images/graph/Representations/NodeSet.dot