.. _DescribedValueDomain:

DescribedValueDomain
********************
A Value Domain defined by an expression. [GSIM 1.1]

Extends
=======
:ref:`ValueDomain`



Graph
=====

.. graphviz:: /images/graph/Representations/DescribedValueDomain.dot