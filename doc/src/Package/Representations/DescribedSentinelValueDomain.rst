.. _DescribedSentinelValueDomain:

DescribedSentinelValueDomain
****************************
A described value domain whose values are used only for the processing of data after capture but before dissemination.

Extends
=======
:ref:`DescribedValueDomain`



Graph
=====

.. graphviz:: /images/graph/Representations/DescribedSentinelValueDomain.dot