.. _Map:

Map
***


Extends
=======
:ref:`OrderedMemberCorrespondence`


Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "validFrom", "Date", "0..1"
   "validTo", "Date", "0..1"


validFrom
#########
Date from which the Map became valid. The date must be defined if the Map belongs to a floating CorrespondenceTable.


validTo
#######
Date at which the Map became invalid. The date must be defined if the Map belongs to a floating Correspondence Table and is no longer valid.




Graph
=====

.. graphviz:: /images/graph/Representations/Map.dot