.. _Node:

Node
****
A combination of a category and related attributes.


Extends
=======
:ref:`Member`



Graph
=====

.. graphviz:: /images/graph/Representations/Node.dot