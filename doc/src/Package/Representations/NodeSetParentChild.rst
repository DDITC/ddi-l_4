.. _NodeSetParentChild:

NodeSetParentChild
******************
Parent-child specialization of OrderRelation between NodeSets.
The inherited type property is set to "partial" to specify that the parent-child relationships among NodeSets define a tree structure.


Extends
=======
:ref:`OrderRelation`



Graph
=====

.. graphviz:: /images/graph/Representations/NodeSetParentChild.dot