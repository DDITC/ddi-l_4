***************
Representations
***************
Logical Data Description covers the logical content of a dataset - the "variable cascade".
Contents 

.. toctree::
   :maxdepth: 2

   CategorySet
   ClassificationFamily
   ClassificationIndex
   ClassificationIndexEntry
   ClassificationItem
   ClassificationSeries
   Code
   CodeList
   CorrespondenceTable
   DataType
   DescribedSentinelValueDomain
   DescribedSubstantiveValueDomain
   DescribedValueDomain
   Designation
   EnumeratedSentinelValueDomain
   EnumeratedSubstantiveValueDomain
   EnumeratedValueDomain
   IndexOrder
   Level
   LevelParentChild
   Map
   Node
   NodeParentChild
   NodePartWhole
   NodeSet
   NodeSetParentChild
   NodeSetPartWhole
   Sign
   StatisticalClassification
   ValueDomain
   Vocabulary
