***********
BaseObjects
***********
The package is a holding place for objects that are widely used but are not treated as properties; primarily identifiable types. A decision on the status and location of these objects needs to be made.
Contents 

.. toctree::
   :maxdepth: 2

