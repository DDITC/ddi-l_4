.. _Agent:

Agent
*****
An actor that performs a role in relation to a process. 

Extends
=======
:ref:`AnnotatedIdentifiable`


Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "agentId", "AgentId", "0..n"
   "description", "StructuredString", "0..1"


agentId
#######
An identifier within a specified system for specifying an agent


description
###########
Multilingual description allowing for internal formatting using XHTML tags.




Graph
=====

.. graphviz:: /images/graph/Agents/Agent.dot