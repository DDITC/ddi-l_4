******
Agents
******
Agents Package covers the description of Organizations, Individuals, and Machines (non-human) and their relationships over time. Agents are related to by processes, annotation content, and other locations where individuals, organizations, or machines are involved with the activities covered by DDI metadata.
Contents 

.. toctree::
   :maxdepth: 2

   Agent
   AuthorizationSource
   Individual
   Machine
   Organization
   Relation
