.. _Organization:

Organization
************
A framework of authority designated to act toward some purpose.

Extends
=======
:ref:`Agent`


Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "organizationName", "OrganizationName", "1..n"
   "imageURL", "PrivateImage", "0..n"
   "ddiId", "xs:string", "0..n"
   "contactInformation", "ContactInformation", "0..1"


organizationName
################
Names by which the organization is known.


imageURL
########
The URL of an image of the organization.


ddiId
#####
The agency identifier of the organization as registered at the DDI Alliance register.


contactInformation
##################





Graph
=====

.. graphviz:: /images/graph/Agents/Organization.dot