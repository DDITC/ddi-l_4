.. _Machine:

Machine
*******
Mechanism or computer program used to implement a process.

Extends
=======
:ref:`Agent`


Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "typeOfMachine", "CodeValueType", "0..1"
   "machineName", "Name", "0..1"
   "accessLocation", "AccessLocation", "0..1"
   "function", "CodeValueType", "0..n"
   "interface", "CodeValueType", "0..n"
   "imageURL", "PrivateImage", "0..n"
   "ownerOperatorContact", "ContactInformation", "0..1"


typeOfMachine
#############
The kind of machine used - software, web service, physical machine, from a controlled vocabulary


machineName
###########
The name of the machine


accessLocation
##############
The locations where the machine can be access


function
########
The function of the machine


interface
#########



imageURL
########



ownerOperatorContact
####################





Graph
=====

.. graphviz:: /images/graph/Agents/Machine.dot