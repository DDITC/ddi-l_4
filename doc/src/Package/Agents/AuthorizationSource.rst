.. _AuthorizationSource:

AuthorizationSource
*******************
Identifies the authorizing agency and allows for the full text of the authorization (law, regulation, or other form of authorization).

Extends
=======
:ref:`AnnotatedIdentifiable`


Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "statementOfAuthorization", "StructuredString", "0..1"
   "legalMandate", "InternationalString", "0..1"
   "authorizationDate", "xs:dateTime", "0..1"
   "description", "StructuredString", "0..1"


statementOfAuthorization
########################
Text of the authorization (law, mandate, approved business case).


legalMandate
############
Provide a legal citation to a law authorizing the study/data collection. For example, a legal citation for a law authorizing a country's census.


authorizationDate
#################
Identifies the date of Authorization.


description
###########





Graph
=====

.. graphviz:: /images/graph/Agents/AuthorizationSource.dot