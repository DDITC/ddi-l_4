.. _Relation:

Relation
********
Describes the relationship between any two organizations or individuals, or an individual and an organization. This is a pairwise relationship and relationships may be unidirectional. Identifies the Source organization or individual and the Target organization or individual, describes the relationship, provides a keyword to classify the relationship, provides and effective period for the relationship, allows for addition information to be provided, and can contain a privacy specification.

Extends
=======
:ref:`AnnotatedIdentifiable`


Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "description", "StructuredString", "0..1"
   "effectivePeriod", "Date", "0..n"
   "privacy", "CodeValueType", "0..1"
   "typeOfRelationship", "InternationalCodeValueType", "0..1"


description
###########
A description of the relationship. May be expressed in multiple languages and supports the use of structured content.


effectivePeriod
###############
Time period during which this relationship is valid.


privacy
#######
Specifies the level of privacy for the relationship specification as public, restricted, or private. Supports the use of an external controlled vocabulary.


typeOfRelationship
##################
A brief textual identification of the type of relation. Supports the use of an external controlled vocabulary.




Graph
=====

.. graphviz:: /images/graph/Agents/Relation.dot