.. _Individual:

Individual
**********
A person who acts, or is designated to act towards a specific purpose.

Extends
=======
:ref:`Agent`


Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "individualName", "IndividualName", "1..n"
   "imageURL", "PrivateImage", "0..n"
   "ddiId", "xs:string", "0..n"
   "contactInformation", "ContactInformation", "0..1"


individualName
##############
The name of an individual broken out into its component parts of prefix, first/given name, middle name, last/family/surname, and suffix.


imageURL
########
The URL of an image of the individual.


ddiId
#####
The agency identifier of the individual according to the DDI Alliance agent registry.


contactInformation
##################





Graph
=====

.. graphviz:: /images/graph/Agents/Individual.dot