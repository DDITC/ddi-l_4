.. _Binding:

Binding
*******
A structure used to bind the content of a parameter declared as the source to a parameter declared as the target. For example, binding the output of a question to the input of a generation instruction. Question A has an OutParameter X. Generation Instruction has an InParameter Y used in the recode instruction. Binding defines the content of InParameter Y to be whatever is provided by OutParameter X for use in the calculation of the recode.



Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "sourceParameter", "xs:string", "1..1"
   "targetParameter", "xs:string", "1..1"


sourceParameter
###############
A structure used to bind the content of a parameter declared as the source to a parameter declared as the target. For example, binding the output of a question to the input of a generation instruction. Question A has an OutParameter X. Generation Instruction has an InParameter Y used in the recode instruction. Binding defines the content of InParameter Y to be whatever is provided by OutParameter X for use in the calculation of the recode.[Referenced object not explicit]


targetParameter
###############
A structure used to bind the content of a parameter declared as the source to a parameter declared as the target. For example, binding the output of a question to the input of a generation instruction. Question A has an OutParameter X. Generation Instruction has an InParameter Y used in the recode instruction. Binding defines the content of InParameter Y to be whatever is provided by OutParameter X for use in the calculation of the recode.[Referenced object not explicit]




Graph
=====

.. graphviz:: /images/graph/CoreProcess/Binding.dot