.. _OverlapsTemporalRelation:

OverlapsTemporalRelation
************************


Extends
=======
:ref:`TemporalRelation`



Graph
=====

.. graphviz:: /images/graph/CoreProcess/OverlapsTemporalRelation.dot