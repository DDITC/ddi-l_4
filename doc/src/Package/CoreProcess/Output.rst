.. _Output:

Output
******
Output to a process step, either a type of an object or an instance.

Extends
=======
:ref:`Parameter`



Graph
=====

.. graphviz:: /images/graph/CoreProcess/Output.dot