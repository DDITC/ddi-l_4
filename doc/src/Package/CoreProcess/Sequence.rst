.. _Sequence:

Sequence
********
Provides a sequence order for operations expressed as control constructs. The sequence can be typed to support local processing or classification flags and alternate sequencing instructions (such as randomize for each respondent).

Extends
=======
:ref:`ControlConstruct`


Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "typeOfSequence", "CodeValueType", "0..n"
   "constructSequence", "SpecificSequence", "0..1"


typeOfSequence
##############
Provides the ability to "type" a sequence for classification or processing purposes. Supports the use of an external controlled vocabulary.


constructSequence
#################
Describes alternate ordering for different cases using the SpecificSequence structure. If you set the sequence to anything other than order of appearance the only allowable children are QuestionConstruct or Sequence. Contents must be randomizable.




Graph
=====

.. graphviz:: /images/graph/CoreProcess/Sequence.dot