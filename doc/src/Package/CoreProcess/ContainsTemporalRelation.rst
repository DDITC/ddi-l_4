.. _ContainsTemporalRelation:

ContainsTemporalRelation
************************


Extends
=======
:ref:`TemporalRelation`



Graph
=====

.. graphviz:: /images/graph/CoreProcess/ContainsTemporalRelation.dot