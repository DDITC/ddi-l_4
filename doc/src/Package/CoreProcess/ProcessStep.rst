.. _ProcessStep:

ProcessStep
***********
Work package performed by a service to transform inputs to outputs considering rules as defined in the control construct.

Extends
=======
:ref:`AnnotatedIdentifiable`


Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "label", "Label", "0..1"
   "description", "StructuredString", "0..1"
   "definition", "StructuredString", "0..1"
   "binding", "Binding", "0..1"


label
#####



description
###########



definition
##########



binding
#######
A structure used to bind the content of a parameter declared as the source to a parameter declared as the target. The binding may be defined as part of the ProcessStep in which case it is called "in line". The binding may also be defined by a ProcessStep user such as an Instance Variable which might get its value from the ProcessStep. In this case we say the ProcessStep or a set of ProcessSteps that form a processing pipeline is reusable and "declarative" only.




Graph
=====

.. graphviz:: /images/graph/CoreProcess/ProcessStep.dot