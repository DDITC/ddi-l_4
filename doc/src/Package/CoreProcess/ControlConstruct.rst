.. _ControlConstruct:

ControlConstruct
****************
A ControlConstruct is used in the definition of the sequence of execution of process steps.

Extends
=======
:ref:`ProcessStep`



Graph
=====

.. graphviz:: /images/graph/CoreProcess/ControlConstruct.dot