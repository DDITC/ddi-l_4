.. _Input:

Input
*****
Input to a process step, either a type of an object or an instance.

Extends
=======
:ref:`Parameter`


Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "limitArrayIndex", "xs:NMTOKENS", "0..1"


limitArrayIndex
###############
When the Input represents an array of items, this attribute specifies the index identification of the items within the zero-based array which should be treated as input parameters. If not specified, the full array is treated as the input parameter.




Graph
=====

.. graphviz:: /images/graph/CoreProcess/Input.dot