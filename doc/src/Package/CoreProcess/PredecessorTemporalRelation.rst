.. _PredecessorTemporalRelation:

PredecessorTemporalRelation
***************************


Extends
=======
:ref:`TemporalRelation`



Graph
=====

.. graphviz:: /images/graph/CoreProcess/PredecessorTemporalRelation.dot