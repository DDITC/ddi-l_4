.. _Loop:

Loop
****
Describes an action which loops until a limiting condition is met.

Extends
=======
:ref:`ControlConstruct`


Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "initialValue", "xs:integer", "0..1"
   "loopWhile", "CommandCode", "0..1"
   "stepValue", "xs:integer", "0..1"


initialValue
############
The command used to set the initial value for the process. Could be a simple value.


loopWhile
#########
The command used to determine whether the "LoopWhile" condition is met.


stepValue
#########
The command used to set the incremental or step value for the process. Could be a simple value.




Graph
=====

.. graphviz:: /images/graph/CoreProcess/Loop.dot