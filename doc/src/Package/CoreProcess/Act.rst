.. _Act:

Act
***
An Act is a type of ControlConstruct. An Act has many subtypes including an Instruction, a Question, an Instrument and a StudyUnit. Both Acts and ControlConstructs are triggered when the conditions of a ControlConstruct are met.

Extends
=======
:ref:`ControlConstruct`



Graph
=====

.. graphviz:: /images/graph/CoreProcess/Act.dot