.. _Service:

Service
*******
A means of performing a Business Function (an ability that an organization possesses, typically expressed in general and high level terms and requiring a combination of organization, people, processes and technology to achieve). 
(source: GSIM)

Extends
=======
:ref:`ProcessStep`


Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "interface", "CodeValueType", "0..1"
   "location", "CodeValueType", "0..1"


interface
#########
Specifies how to communicate with the service.


location
########
Specifies where the service can be accessed.




Graph
=====

.. graphviz:: /images/graph/CoreProcess/Service.dot