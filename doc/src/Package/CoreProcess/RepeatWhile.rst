.. _RepeatWhile:

RepeatWhile
***********
Specifies a ControlConstruct to be repeated while a specified condition is met. Before each iteration the condition is tested. When the condition is not met, control passes back to the containing control construct.

Extends
=======
:ref:`ControlConstruct`


Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "whileCondition", "CommandCode", "1..1"


whileCondition
##############
Information on the command used to determine whether the "While" condition is met.




Graph
=====

.. graphviz:: /images/graph/CoreProcess/RepeatWhile.dot