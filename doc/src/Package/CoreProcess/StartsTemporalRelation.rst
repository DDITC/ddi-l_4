.. _StartsTemporalRelation:

StartsTemporalRelation
**********************


Extends
=======
:ref:`TemporalRelation`



Graph
=====

.. graphviz:: /images/graph/CoreProcess/StartsTemporalRelation.dot