***********
CoreProcess
***********
CoreProcess model for DDI 4. The CoreProcess contains a set of objects that serve as the basis for describing specific processes using DDI object. It is intended to be specialized to support specific applications. Some examples of its use can be seen in DataCapture, HistoricalProcess, and PrescriptiveProcess.
Contents 

.. toctree::
   :maxdepth: 2

   Act
   Binding
   ContainsTemporalRelation
   ControlConstruct
   EqualTemporalRelation
   FinishesTemporalRelation
   IfThenElse
   Input
   Loop
   MeetsTemporalRelation
   Output
   OverlapsTemporalRelation
   PredecessorTemporalRelation
   ProcessStep
   RepeatUntil
   RepeatWhile
   Sequence
   Service
   StartsTemporalRelation
   TemporalRelation
