.. _IfThenElse:

IfThenElse
**********
IfThenElse describes an if-then-else decision type of control construct. IF the stated condition is met, the THEN clause is trigged, otherwise the ELSE clause is triggered.


Extends
=======
:ref:`ControlConstruct`


Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "ifCondition", "CommandCode", "1..1"


ifCondition
###########
The condition which must be met to trigger the Then clause, expressed as a CommandCode. The condition is an expression in the programming language used in the instrument.




Graph
=====

.. graphviz:: /images/graph/CoreProcess/IfThenElse.dot