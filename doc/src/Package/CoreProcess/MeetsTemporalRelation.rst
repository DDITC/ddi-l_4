.. _MeetsTemporalRelation:

MeetsTemporalRelation
*********************


Extends
=======
:ref:`TemporalRelation`



Graph
=====

.. graphviz:: /images/graph/CoreProcess/MeetsTemporalRelation.dot