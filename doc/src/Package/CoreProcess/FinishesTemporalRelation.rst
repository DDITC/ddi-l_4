.. _FinishesTemporalRelation:

FinishesTemporalRelation
************************


Extends
=======
:ref:`TemporalRelation`



Graph
=====

.. graphviz:: /images/graph/CoreProcess/FinishesTemporalRelation.dot