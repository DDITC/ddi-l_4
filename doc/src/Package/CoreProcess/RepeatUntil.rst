.. _RepeatUntil:

RepeatUntil
***********
Specifies a ControlConstruct to be repeated until a specified condition is met. Before each iteration the condition is tested. When the condition is met, control passes back to the containing control construct.

Extends
=======
:ref:`ControlConstruct`


Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "untilCondition", "CommandCode", "1..1"


untilCondition
##############
Information on the command used to determine whether the "Until" condition is met.




Graph
=====

.. graphviz:: /images/graph/CoreProcess/RepeatUntil.dot