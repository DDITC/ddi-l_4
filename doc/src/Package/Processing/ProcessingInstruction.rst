.. _ProcessingInstruction:

ProcessingInstruction
*********************


Extends
=======
:ref:`ControlConstruct`


Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "commandCode", "CommandCode", "0..n"


commandCode
###########
Structured information used by a system to process the instruction.




Graph
=====

.. graphviz:: /images/graph/Processing/ProcessingInstruction.dot