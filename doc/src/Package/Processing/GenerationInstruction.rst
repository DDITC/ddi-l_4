.. _GenerationInstruction:

GenerationInstruction
*********************
Processing instructions for recodes, derivations from multiple question or variable sources, and derivations based on external sources. Instructions should be listed separately so they can be referenced individually.

Extends
=======
:ref:`Act`


Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "externalInformation", "AccessRights", "0..n"
   "description", "StructuredString", "0..1"
   "commandCode", "CommandCode", "0..n"
   "isDerived", "xs:boolean", "0..1"


externalInformation
###################
Reference to an external source of information used in the coding process, for example a value from a chart, etc.


description
###########
A description of the generation instruction. May be expressed in multiple languages and supports the use of structured content.


commandCode
###########
Structured information used by a system to process the instruction.


isDerived
#########
Default setting is "true", the instruction describes a derivation. If the instruction is a simple recode, set to "false".




Graph
=====

.. graphviz:: /images/graph/Processing/GenerationInstruction.dot