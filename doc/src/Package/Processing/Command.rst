.. _Command:

Command
*******
Provides the following information on the command The content of the command, the programming language used, the pieces of information (InParameters) used by the command, the pieces of information created by the command (OutParameters) and the source of the information used by the InParameters (Binding).

Extends
=======
:ref:`AnnotatedIdentifiable`


Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "programLanguage", "CodeValueType", "0..1"
   "commandContent", "xs:string", "0..1"


programLanguage
###############
Designates the programming language used for the command. Supports the use of a controlled vocabulary.


commandContent
##############
Content of the command itself expressed in the language designated in Programming Language.




Graph
=====

.. graphviz:: /images/graph/Processing/Command.dot