.. _Parameter:

Parameter
*********
A parameter is a structure that specifically identifies a source of input or output information so that it can be use pragmatically.

Extends
=======
:ref:`AnnotatedIdentifiable`


Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "alias", "xs:NMTOKEN", "0..1"
   "defaultValue", "Value", "0..1"
   "isArray", "xs:boolean", "0..1"


alias
#####
If the content of the parameter is being used in a generic set of code as an alias for the value of an object in a formula (for example source code for a statistical program) enter that name here. This provides a link from the identified parameter to the alias in the code.


defaultValue
############
Provides a default value for the parameter if there is no value provided by the object it is bound to or the process that was intended to produce a value.


isArray
#######
If set to "true" indicates that the content of the parameter is a delimited array rather than a single object and should be processed as such.




Graph
=====

.. graphviz:: /images/graph/Processing/Parameter.dot