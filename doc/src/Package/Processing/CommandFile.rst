.. _CommandFile:

CommandFile
***********
Identifies and provides a link to an external copy of the command, for example, a SAS Command Code script. Designates the programming language of the command file, designates input and output parameters, binding information between input and output parameters, a description of the location of the file , and a URN or URL for the command file.

Extends
=======
:ref:`AnnotatedIdentifiable`


Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "programLanguage", "CodeValueType", "0..1"
   "location", "InternationalString", "0..1"
   "uri", "xs:anyURI", "0..1"


programLanguage
###############
Designates the programming language used for the command. Supports the use of a controlled vocabulary.


location
########
A description of the location of the file. This may not be machine actionable. It supports a description expressed in multiple languages.


uri
###
The URL or URN of the command file.




Graph
=====

.. graphviz:: /images/graph/Processing/CommandFile.dot