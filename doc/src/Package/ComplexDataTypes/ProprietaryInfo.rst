.. _ProprietaryInfo:

ProprietaryInfo
***************
Contains information proprietary to the software package which produced the data file. This is expressed as a set of key(name)-value pairs.



Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "proprietaryProperty", "StandardKeyValuePair", "0..n"


proprietaryProperty
###################
A structure that supports the use of a standard key value pair. Note that this information is often not interoperable and is provided to support the use of the metadata within specific systems.




Graph
=====

.. graphviz:: /images/graph/ComplexDataTypes/ProprietaryInfo.dot