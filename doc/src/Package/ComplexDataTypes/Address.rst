.. _Address:

Address
*******
Location address identifying each part of the address as separate elements, identifying the type of address, the level of privacy associated with the release of the address, and a flag to identify the preferred address for contact.



Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "typeOfAddress", "CodeValueType", "0..1"
   "line", "xs:string", "0..n"
   "cityPlaceLocal", "xs:string", "0..1"
   "stateProvince", "xs:string", "0..1"
   "postalCode", "xs:string", "0..1"
   "countryCode", "CountryCode", "0..1"
   "timeZone", "CodeValueType", "0..1"
   "effectivePeriod", "xs:date", "0..1"
   "privacy", "CodeValueType", "0..1"
   "isPreferred", "xs:boolean", "0..1"
   "geographicPoint", "Point", "0..1"
   "regionalCoverage", "CodeValueType", "0..1"
   "typeOfLocation", "CodeValueType", "0..1"
   "locationName", "Name", "0..1"


typeOfAddress
#############
Indicates address type (i.e. home, office, mailing, etc.)


line
####
Number and street including office or suite number. May use multiple lines.


cityPlaceLocal
##############
City, place, or local area used as part of an address.


stateProvince
#############
A major subnational division such as a state or province used to identify a major region within an address.


postalCode
##########
Postal or ZIP Code


countryCode
###########
Country of the location


timeZone
########
Time zone of the location expressed as code.


effectivePeriod
###############
Clarifies when the identification information is accurate.


privacy
#######
Specify the level privacy for the address as public, restricted, or private. Supports the use of an external controlled vocabulary


isPreferred
###########
Set to "true" if this is the preferred location for contacting the organization or individual.


geographicPoint
###############
Geographic coordinates corresponding to the address.


regionalCoverage
################
The region covered by the agent at this address


typeOfLocation
##############
The type or purpose of the location (i.e. regional office, distribution center, home)


locationName
############
Name of the location if applicable.




Graph
=====

.. graphviz:: /images/graph/ComplexDataTypes/Address.dot