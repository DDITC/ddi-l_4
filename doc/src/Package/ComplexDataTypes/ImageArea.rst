.. _ImageArea:

ImageArea
*********
Defines the shape and area of an image used as part of a location representation. The shape is defined as a Rectangle, Circle, or Polygon and Coordinates provides the information required to define it.



Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "coordinates", "xs:string", "0..1"
   "shape", "ShapeCoded", "1..1"


coordinates
###########
A comma-delimited list of x,y coordinates, listed as a set of adjacent points for rectangles and polygons, and as a center-point and a radius for circles (x,y,r).


shape
#####
A fixed set of valid responses includes Rectangle, Circle, and Polygon.




Graph
=====

.. graphviz:: /images/graph/ComplexDataTypes/ImageArea.dot