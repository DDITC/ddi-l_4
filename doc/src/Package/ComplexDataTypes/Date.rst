.. _Date:

Date
****
Provides the structure of a Date element, which allows a choice between single, simple dates (of BaseDateType) or date ranges. If the Date element contains a range, Cycle may be used to indicate occurrence of this range within a series of ranges as an integer identifying the cycle, i.e. the 4th wave of a data collection cycle would have. BaseDateType allows for different date time combinations to provide a simple and convenient mechanism to specify different date and time values with a machine actionable format specified by regular expressions.



Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "simpleDate", "BaseDateType", "0..1"
   "historicalDate", "HistoricalDate", "0..1"
   "startDate", "BaseDateType", "0..1"
   "historicalStartDate", "HistoricalDate", "0..1"
   "endDate", "BaseDateType", "0..1"
   "historicalEndDate", "HistoricalDate", "0..1"
   "cycle", "xs:integer", "0..1"


simpleDate
##########
A single point in time. If a duration is expressed as a SimpleDate it is defining a period of time without a designated Start or End date.


historicalDate
##############
A simple date expressed in a historical date format, including a specification of the date format and calendar used.


startDate
#########
Start of a date range. If there is a start date with no end date provided, this implies that the end date is unknown but that the date being recorded is not just a simple date but a range of unknown duration.


historicalStartDate
###################
A start date expressed in a historical date format, including a specification of the date format and calendar used.


endDate
#######
End of a date range which may or may not have a known start date.


historicalEndDate
#################
An end date expressed in a historical date format, including a specification of the date format and calendar used.


cycle
#####
Use to indicate occurrence of this range within a series of ranges as an integer identifying the cycle, i.e. the 4th wave of a data collection cycle would have




Graph
=====

.. graphviz:: /images/graph/ComplexDataTypes/Date.dot