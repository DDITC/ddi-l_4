.. _BasedOnObject:

BasedOnObject
*************
Use when creating an object that is based on an existing object or objects that are managed by a different agency or when the new object is NOT simply a version change but you wish to maintain a reference to the object that served as a basis for the new object. BasedOnObject may contain references to any number of objects which serve as a basis for this object, a BasedOnRationaleDescription of how the content of the referenced object was incorporated or altered, and a BasedOnRationaleCode to allow for specific typing of the BasedOnReference according to an external controlled vocabulary.



Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "basedOnRationaleDescription", "InternationalString", "0..1"
   "basedOnRationaleCode", "CodeValueType", "0..1"


basedOnRationaleDescription
###########################
Textual description of the rationale/purpose for the based on reference to inform users as to the extent and implication of the version change. May be expressed in multiple languages.


basedOnRationaleCode
####################
RationaleCode is primarily for internal processing flags within an organization or system. Supports the use of an external controlled vocabulary.




Graph
=====

.. graphviz:: /images/graph/ComplexDataTypes/BasedOnObject.dot