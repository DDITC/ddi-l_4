.. _LocalId:

LocalId
*******
This is an identifier in a given local context that uniquely references an object, as opposed to the full ddi identifier which has an agency plus the id.



Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "localIdValue", "xs:string", "1..1"
   "localIdType", "xs:string", "1..1"
   "localIdVersion", "xs:string", "0..1"


localIdValue
############
Value of the local ID.


localIdType
###########
Type of identifier, specifying the context of the identifier.


localIdVersion
##############
Version of the Local ID.




Graph
=====

.. graphviz:: /images/graph/ComplexDataTypes/LocalId.dot