.. _Content:

Content
*******
Supports the optional use of XHTML formatting tags within the string structure. XHTML tag content is controlled by the schema, see http://www.w3.org/1999/xhtml/ for a detailed list of available tags. Language of the string is defined by the attribute xmlang. The content can be identified as translated (isTranslated), subject to translation (isTranslatable), the result of translation from one or more languages (translationSourceLanguages), and carry an indication whether or not it should be treated as plain text (isPlain).



Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "content", "xhtml:BlkNoForm.mix", "1..n"
   "xmlLang", "xs:language", "0..1"
   "isTranslated", "xs:boolean", "0..1"
   "isTranslatable", "xs:boolean", "0..1"
   "translationSourceLanguage", "xs:language", "0..n"
   "translationDate", "xs:date", "0..1"
   "isPlainText", "xs:boolean", "0..1"


content
#######
The following xhtml tags are available for use in Content: address, blockquote, pre, h1, h2, h3, h4, h5, h6, hr, div, p, a, abbr, acronym, cite, code, dfn, em, kbd, q, samp, strong, var, b, big, i, small, sub, sup, tt, br, span, dl, dt, dd, ol, ul, li, table, caption, thead, tfoot, tbody, colgroup, col, tr, th, and td. They should be used with the xhtml namespace prefix, i.e., xhtmdiv. See DDI Technical Manual Part I for additional details.


xmlLang
#######
Indicates the language of content.


isTranslated
############
Indicates whether content is a translation (true) or an original (false).


isTranslatable
##############
Indicates whether content is translatable (true) or not (false).


translationSourceLanguage
#########################
List the language or language codes in a space delimited array. The language original may or may not be provided in this bundle of language specific strings.


translationDate
###############
The date the content was translated. Provision of translation date allows user to verify if translation was available during data collection or other time linked activity.


isPlainText
###########
Indicates that the content is to be treated as plain text (no formatting). You may use DDIProfile to fix the value of this attribute to 'true' in cases where you wish to indicate that your system treats all content should be treated as plain text.




Graph
=====

.. graphviz:: /images/graph/ComplexDataTypes/Content.dot