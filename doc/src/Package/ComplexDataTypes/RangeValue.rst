.. _RangeValue:

RangeValue
**********
Describes a bounding value of a string.

Extends
=======
:ref:`Value`


Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "included", "xs:boolean", "0..1"


included
########
Set to "true" if the value is included in the range.




Graph
=====

.. graphviz:: /images/graph/ComplexDataTypes/RangeValue.dot