.. _VideoSegment:

VideoSegment
************
Describes the type and length of the video segment.



Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "typeOfVideoClip", "CodeValueType", "0..1"
   "videoClipBegin", "xs:string", "0..1"
   "videoClipEnd", "xs:string", "0..1"


typeOfVideoClip
###############
The type of video clip provided. Supports the use of a controlled vocabulary.


videoClipBegin
##############
The point to begin the video clip. If no point is provided the assumption is that the start point is the beginning of the clip provided.


videoClipEnd
############
The point to end the video clip. If no point is provided the assumption is that the end point is the end of the clip provided.




Graph
=====

.. graphviz:: /images/graph/ComplexDataTypes/VideoSegment.dot