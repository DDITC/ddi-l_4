.. _Segment:

Segment
*******
A structure used to express explicit segments or regions within different types of external materials (Textual, Audio, Video, XML, and Image). Provides the appropriate start, stop, or region definitions for each type.



Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "audioSegment", "AudioSegment", "0..n"
   "videoSegment", "VideoSegment", "0..n"
   "xml", "xs:string", "0..n"
   "textualSegment", "TextualSegment", "0..n"
   "imageArea", "ImageArea", "0..n"


audioSegment
############
Describes the type and length of the audio segment.


videoSegment
############
Describes the type and length of the video segment.


xml
###
An X-Pointer expression identifying a node in the XML document.


textualSegment
##############
Defines the segment of textual content used by the parent object. Can identify a set of lines and or characters used to define the segment


imageArea
#########
Defines the shape and area of an image used as part of a location representation. The shape is defined as a Rectangle, Circle, or Polygon and Coordinates provides the information required to define it.




Graph
=====

.. graphviz:: /images/graph/ComplexDataTypes/Segment.dot