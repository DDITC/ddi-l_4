.. _InternationalIdentifier:

InternationalIdentifier
***********************
An identifier whose scope of uniqueness is broader than the local archive. Common forms of an international identifier are ISBN, ISSN, DOI or similar designator. Provides both the value of the identifier and the agency who manages it.



Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "identifierContent", "xs:string", "0..1"
   "managingAgency", "CodeValueType", "0..1"
   "isURI", "xs:boolean", "0..1"


identifierContent
#################
An identifier as it should be listed for identification purposes.


managingAgency
##############
The identification of the Agency which assigns and manages the identifier, i.e., ISBN, ISSN, DOI, etc.


isURI
#####
Set to "true" if Identifier is a URI




Graph
=====

.. graphviz:: /images/graph/ComplexDataTypes/InternationalIdentifier.dot