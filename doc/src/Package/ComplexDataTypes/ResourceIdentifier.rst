.. _ResourceIdentifier:

ResourceIdentifier
******************
Provides a means of identifying a related resource and provides the typeOfRelationship. Makes use of a controlled vocabulary for typing the relationship. Standard usage may include: describesDate, isDescribedBy,  isFormatOf, isPartOf, isReferencedBy, isReplacedBy, isRequiredBy, isVersionOf, references, replaces, requires, etc.



Extends
=======
:ref:`InternationalIdentifier`


Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "typeOfRelatedResource", "CodeValueType", "0..n"


typeOfRelatedResource
#####################
The type of relationship between the annotated object and the related resource. Standard usage may include: describesDate, isDescribedBy,  isFormatOf, isPartOf, isReferencedBy, isReplacedBy, isRequiredBy, isVersionOf, references, replaces, requires, etc.




Graph
=====

.. graphviz:: /images/graph/ComplexDataTypes/ResourceIdentifier.dot