.. _TextContent:

TextContent
***********
Abstract type existing as the head of a substitution group. May be replaced by any valid member of the substitution group TextContent. Provides the common element Description to all members using TextContent as an extension base.



Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "description", "StructuredString", "0..1"


description
###########
A description of the content and purpose of the text segment. May be expressed in multiple languages and supports the use of structured content.




Graph
=====

.. graphviz:: /images/graph/ComplexDataTypes/TextContent.dot