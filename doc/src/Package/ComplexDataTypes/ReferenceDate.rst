.. _ReferenceDate:

ReferenceDate
*************
The date covered by the annotated object. In addition to specifying a type of date (e.g. collection period, census year, etc.) the date or time span may be associated with a particular subject or keyword. This allows for the expression of a referent date associated with specific subjects or keywords. For example, a set of date items on income and labor force status may have a referent date for the year prior to the collection date.

Extends
=======
:ref:`AnnotationDate`


Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "subject", "InternationalCodeValueType", "0..n"
   "keyword", "InternationalCodeValueType", "0..n"


subject
#######
If the date is for a subset of data only such as a referent date for residence 5 years ago, use Subject to specify the coverage of the data this date applies to. May be repeated to reflect multiple subjects.


keyword
#######
If the date is for a subset of data only such as a referent date for residence 5 years ago, use keyword to specify the coverage of the data this date applies to. May be repeated to reflect multiple keywords.




Graph
=====

.. graphviz:: /images/graph/ComplexDataTypes/ReferenceDate.dot