.. _CorrespondenceType:

CorrespondenceType
******************
Describes the commonalities and differences between two items using a textual description of both commonalities and differences plus an optional coding of the type of commonality.



Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "commonality", "StructuredString", "0..1"
   "difference", "StructuredString", "0..1"
   "commonalityTypeCode", "CodeValueType", "0..n"


commonality
###########
A description of the common features of the two items using a StructuredString to support multiple language versions of the same content as well as optional formatting of the content.


difference
##########
A description of the differences between the two items using a StructuredString to support multiple language versions of the same content as well as optional formatting of the content.


commonalityTypeCode
###################
Commonality expressed as a term or code. Supports the use of an external controlled vocabulary. If repeated, clarify each external controlled vocabulary used.




Graph
=====

.. graphviz:: /images/graph/ComplexDataTypes/CorrespondenceType.dot