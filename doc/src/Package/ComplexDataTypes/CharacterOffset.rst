.. _CharacterOffset:

CharacterOffset
***************
Specification of the character offset for the beginning and end of the segment.



Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "startCharOffset", "xs:integer", "0..1"
   "endCharOffset", "xs:integer", "0..1"


startCharOffset
###############
Number of characters from beginning of the document, indicating the inclusive start of the text range.


endCharOffset
#############
Number of characters from the beginning of the document, indicating the inclusive end of the text segment.




Graph
=====

.. graphviz:: /images/graph/ComplexDataTypes/CharacterOffset.dot