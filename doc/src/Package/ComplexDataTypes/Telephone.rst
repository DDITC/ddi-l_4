.. _Telephone:

Telephone
*********
Details of a telephone number including the number, type of number, a privacy setting and an indication of whether this is the preferred contact number.



Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "telephoneNumber", "xs:string", "0..1"
   "typeOfTelephone", "CodeValueType", "0..1"
   "effectivePeriod", "Date", "0..1"
   "privacy", "CodeValueType", "0..1"
   "isPreferred", "xs:boolean", "0..1"


telephoneNumber
###############
The telephone number including country code if appropriate.


typeOfTelephone
###############
Indicates type of telephone number provided (home, fax, office, cell, etc.). Supports the use of a controlled vocabulary.


effectivePeriod
###############
Time period during which the telephone number is valid.


privacy
#######
Specify the level privacy for the telephone number as public, restricted, or private. Supports the use of an external controlled vocabulary.


isPreferred
###########
Set to "true" if this is the preferred telephone number for contact.




Graph
=====

.. graphviz:: /images/graph/ComplexDataTypes/Telephone.dot