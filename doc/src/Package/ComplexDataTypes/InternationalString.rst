.. _InternationalString:

InternationalString
*******************
Packaging structure for multiple language versions of the same string content. Where an element of this type is repeatable, the expectation is that each repetition contains different content, each of which can be expressed in multiple languages. The language designation goes on the individual String.



Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "string", "String", "0..n"


string
######
A non-formatted string of text with an attribute that designates the language of the text. Repeat this object to express the same content in another language.




Graph
=====

.. graphviz:: /images/graph/ComplexDataTypes/InternationalString.dot