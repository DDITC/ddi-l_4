.. _ConditionalText:

ConditionalText
***************
Text which has a changeable value depending on a stated condition, response to earlier questions, or as input from a set of metrics (pre-supplied data).

Extends
=======
:ref:`TextContent`


Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "expression", "CommandCode", "0..1"


expression
##########
The condition on which the associated text varies expressed by a command code. For example, a command that inserts an age by calculating the difference between today’s date and a previously defined date of birth.




Graph
=====

.. graphviz:: /images/graph/ComplexDataTypes/ConditionalText.dot