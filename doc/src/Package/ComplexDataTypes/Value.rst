.. _Value:

Value
*****
The Value expressed as an xs:string with the ability to preserve whitespace if critical to the understanding of the content.



Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "content", "xs:string", "1..1"
   "whiteSpace", "WhiteSpace", "0..1"


content
#######
The actual content of this value as a string


whiteSpace
##########
The default setting states that leading and trailing white space will be removed and multiple adjacent white spaces will be treated as a single white space. If the existence of any of these white spaces is critical to the understanding of the content, change the value of this attribute to "preserve".




Graph
=====

.. graphviz:: /images/graph/ComplexDataTypes/Value.dot