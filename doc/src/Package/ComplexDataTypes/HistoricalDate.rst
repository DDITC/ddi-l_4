.. _HistoricalDate:

HistoricalDate
**************
Used to preserve an historical date, formatted in a non-ISO fashion.



Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "nonISODate", "xs:string", "1..1"
   "historicalDateFormat", "CodeValueType", "0..1"
   "calendar", "CodeValueType", "0..1"


nonISODate
##########
This is the date expressed in a non-ISO compliant structure. Primarily used to retain legacy content or to express non-Gregorian calender dates.


historicalDateFormat
####################
Indicate the structure of the date provided in NonISODate. For example if the NonISODate contained 4/1/2000 the Historical Date Format would be mm/dd/yyyy. The use of a controlled vocabulary is strongly recommended to support interoperability.


calendar
########
Specifies the type of calendar used (e.g., Gregorian, Julian, Jewish).




Graph
=====

.. graphviz:: /images/graph/ComplexDataTypes/HistoricalDate.dot