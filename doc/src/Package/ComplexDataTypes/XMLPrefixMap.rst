.. _XMLPrefixMap:

XMLPrefixMap
************
Maps a specified prefix to a namespace. For each XML namespace used in the profile's XPath expressions, the XML namespaces must have their prefix specified using this element.



Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "xmlPrefix", "xs:string", "0..1"
   "xmlNamespace", "xs:string", "0..1"


xmlPrefix
#########
Specify the exact prefix used.


xmlNamespace
############
Specify the namespace which the prefix represents.




Graph
=====

.. graphviz:: /images/graph/ComplexDataTypes/XMLPrefixMap.dot