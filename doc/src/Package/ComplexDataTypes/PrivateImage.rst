.. _PrivateImage:

PrivateImage
************
References an image using the standard Image description. In addition to the standard attributes provides an effective date (period), the type of image, and a privacy ranking.

Extends
=======
:ref:`Image`


Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "effectivePeriod", "Date", "0..1"
   "privacy", "CodeValueType", "0..1"


effectivePeriod
###############
The period for which this image is effective/valid.


privacy
#######
Specify the level privacy for the image as public, restricted, or private. Supports the use of an external controlled vocabulary.




Graph
=====

.. graphviz:: /images/graph/ComplexDataTypes/PrivateImage.dot