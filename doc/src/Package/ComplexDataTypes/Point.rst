.. _Point:

Point
*****
A geographic point consisting of an X and Y coordinate. Each coordinate value is expressed separately providing its value and format.



Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "xCoordinate", "SpatialCoordinate", "0..1"
   "yCoordinate", "SpatialCoordinate", "0..1"


xCoordinate
###########
An X coordinate (latitudinal equivalent) value and format expressed using the Spatial Coordinate structure.


yCoordinate
###########
A Y coordinate (longitudinal equivalent) value and format expressed using the Spatial Coordinate structure.




Graph
=====

.. graphviz:: /images/graph/ComplexDataTypes/Point.dot