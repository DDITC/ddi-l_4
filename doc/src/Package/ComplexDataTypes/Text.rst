.. _Text:

Text
****
The static portion of the text expressed as a StructuredString with the ability to preserve whitespace if critical to the understanding of the content.

Extends
=======
:ref:`Content`


Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "whiteSpace", "WhiteSpace", "0..1"


whiteSpace
##########
The default setting states that leading and trailing white space will be removed and multiple adjacent white spaces will be treated as a single white space. If the existance of any of these white spaces is critical to the understanding of the content, change the value of this attribute to "preserve".




Graph
=====

.. graphviz:: /images/graph/ComplexDataTypes/Text.dot