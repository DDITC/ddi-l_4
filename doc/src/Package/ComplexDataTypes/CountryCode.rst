.. _CountryCode:

CountryCode
***********
Provides means of expressing a code/term for the country plus an optional valid date.



Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "effectiveDate", "xs:dateTime", "0..1"
   "country", "InternationalCodeValueType", "1..1"


effectiveDate
#############
If it is important to specify the date that this code is effective in order to accurately capture a name or similar change, specify that here.


country
#######
The code or term used to designate the country. If a term, indicate the language.




Graph
=====

.. graphviz:: /images/graph/ComplexDataTypes/CountryCode.dot