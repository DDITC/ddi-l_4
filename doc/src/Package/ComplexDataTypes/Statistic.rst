.. _Statistic:

Statistic
*********
The value of the statistics and whether it is weighted and/or includes missing values.



Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "isWeighted", "xs:boolean", "0..1"
   "computationBase", "ComputationBaseList", "0..1"
   "content", "xs:string", "1..1"


isWeighted
##########
Set to "true" if the statistic is weighted using the weight designated in VariableStatistics.


computationBase
###############
Defines the cases included in determining the statistic. The options are total=all cases, valid and missing (invalid); validOnly=Only valid values, missing (invalid) are not included in the calculation; missingOnly=Only missing (invalid) cases included in the calculation.


content
#######





Graph
=====

.. graphviz:: /images/graph/ComplexDataTypes/Statistic.dot