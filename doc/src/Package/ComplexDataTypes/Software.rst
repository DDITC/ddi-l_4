.. _Software:

Software
********
Describes a specific software package, which may be commercially available or custom-made.



Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "softwareName", "Name", "0..n"
   "softwarePackage", "CodeValueType", "0..1"
   "softwareVersion", "xs:string", "0..1"
   "description", "StructuredString", "0..1"
   "date", "Date", "0..1"
   "function", "CodeValueType", "0..n"
   "xmlLang", "xs:language", "0..1"


softwareName
############
The name of the software package, including its producer.


softwarePackage
###############
A coded value from a controlled vocabulary, describing the software package.


softwareVersion
###############
The version of the software package. Defaults to '1.0'.


description
###########
A description of the content and purpose of the software. May be expressed in multiple languages and supports the use of structured content.


date
####
Supported date of the software package with, at minimum, a release date if known.


function
########
Identifies the functions handled by this software. Repeat for multiple functions. It may be advisable to note only those functions used in the specific usage of the software.


xmlLang
#######
Language (human language) of the software package.




Graph
=====

.. graphviz:: /images/graph/ComplexDataTypes/Software.dot