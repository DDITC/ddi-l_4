.. _AccessLocation:

AccessLocation
**************
A set of access information for a Machine including external and internal URL, mime type, and physical location



Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "externalURLReference", "URL", "0..n"
   "internalURLReference", "xs:anyURI", "0..1"
   "mimeType", "CodeValueType", "0..1"
   "physicalLocation", "InternationalString", "0..n"


externalURLReference
####################
An external URL


internalURLReference
####################
The internal URL.


mimeType
########



physicalLocation
################
The physical location of the machine




Graph
=====

.. graphviz:: /images/graph/ComplexDataTypes/AccessLocation.dot