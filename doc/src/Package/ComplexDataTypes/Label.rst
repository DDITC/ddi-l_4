.. _Label:

Label
*****
A structured display label. Label provides display content of a fully human readable display for the identification of the object. 

Extends
=======
:ref:`StructuredString`


Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "locationVariant", "xs:string", "0..1"
   "validForStartDate", "Date", "0..1"
   "validForEndDate", "Date", "0..1"
   "maxLength", "xs:integer", "0..1"


locationVariant
###############
Indicate the locality specification for content that is specific to a geographic area. May be a country code, sub-country code, or area name.


validForStartDate
#################
Allows for the specification of a starting date for the period that this label is valid. The date must be formatted according to ISO 8601.


validForEndDate
###############
Allows for the specification of a ending date for the period that this label is valid. The date must be formatted according to ISO 8601.


maxLength
#########
A positive integer indicating the maximum number of characters in the label.




Graph
=====

.. graphviz:: /images/graph/ComplexDataTypes/Label.dot