.. _TextualSegment:

TextualSegment
**************
Defines the segment of textual content used by the parent object. Can identify a set of lines and or characters used to define the segment.



Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "lineParamenter", "LineParameter", "1..1"
   "characterParameter", "CharacterOffset", "1..1"


lineParamenter
##############
Specification of the line and offset for the beginning and end of the segment.


characterParameter
##################
Specification of the character offset for the beginning and end of the segment.




Graph
=====

.. graphviz:: /images/graph/ComplexDataTypes/TextualSegment.dot