.. _Form:

Form
****
A link to a form used by the metadata containing the form number, a statement regarding the contents of the form, a statement as to the mandatory nature of the form and a privacy level designation.



Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "formNumber", "xs:string", "0..1"
   "uri", "xs:anyURI", "0..1"
   "statement", "InternationalString", "0..1"
   "isRequired", "xs:boolean", "0..1"


formNumber
##########
The number or other means of identifying the form.


uri
###
The URN or URL of the form.


statement
#########
A statement regarding the use, coverage, and purpose of the form.


isRequired
##########
Set to "true" if the form is required. Set to "false" if the form is optional.




Graph
=====

.. graphviz:: /images/graph/ComplexDataTypes/Form.dot