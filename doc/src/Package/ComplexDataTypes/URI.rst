.. _URI:

URI
***
A URN or URL for a file with a flag to indicate if it is a public copy.



Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "isPublic", "xs:boolean", "0..1"
   "content", "xs:string", "1..1"


isPublic
########
Set to "true" (default value) if this file is publicly available. This does not imply that there are not restrictions to access. Set to "false" if this is not publicly available, such as a backup copy, an internal processing data file, etc.


content
#######





Graph
=====

.. graphviz:: /images/graph/ComplexDataTypes/URI.dot