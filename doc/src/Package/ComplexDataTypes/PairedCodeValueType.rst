.. _PairedCodeValueType:

PairedCodeValueType
*******************
A tightly bound pair of items from a controlled vocabulary. The extent property describes the extent to which the parent term applies for the specific case. 

Extends
=======
:ref:`CodeValueType`


Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "extent", "CodeValueType", "0..1"


extent
######
Describes the extent to which the parent term applies for the specific case using an external controlled vocabulary.




Graph
=====

.. graphviz:: /images/graph/ComplexDataTypes/PairedCodeValueType.dot