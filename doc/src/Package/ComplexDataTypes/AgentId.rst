.. _AgentId:

AgentId
*******
Persistent identifier for a researcher using a system like ORCID



Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "agentIdValue", "xs:string", "1..1"
   "agentIdType", "xs:string", "1..1"


agentIdValue
############
The identifier for the agent.


agentIdType
###########
The identifier system in use.




Graph
=====

.. graphviz:: /images/graph/ComplexDataTypes/AgentId.dot