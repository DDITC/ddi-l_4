.. _Image:

Image
*****
A reference to an image, with a description of its properties and type.



Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "imageLocation", "xs:anyURI", "0..1"
   "typeOfImage", "CodeValueType", "0..1"
   "dpi", "xs:integer", "0..1"
   "languageOfImage", "xs:language", "0..1"


imageLocation
#############
A reference to the location of the image using a URI.


typeOfImage
###########
Brief description of the image type. Supports the use of an external controlled vocabulary.


dpi
###
Provides the resolution of the image in dots per inch to assist in selecting the appropriate image for various uses.


languageOfImage
###############
Language of image.




Graph
=====

.. graphviz:: /images/graph/ComplexDataTypes/Image.dot