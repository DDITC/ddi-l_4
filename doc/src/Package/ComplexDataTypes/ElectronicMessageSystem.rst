.. _ElectronicMessageSystem:

ElectronicMessageSystem
***********************
Any non-email means of relaying a message electronically. This would include text messaging, Skype, Twitter, ICQ, or other emerging means of electronic message conveyance. 



Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "contactAddress", "xs:string", "0..1"
   "typeOfService", "CodeValueType", "0..1"
   "effectivePeriod", "Date", "0..1"
   "privacy", "CodeValueType", "0..1"
   "isPreferred", "xs:boolean", "0..1"


contactAddress
##############
Account identification for contacting


typeOfService
#############
Indicates the type of service used. Supports the use of a controlled vocabulary.


effectivePeriod
###############
Time period during which the account is valid.


privacy
#######
Specify the level privacy for the address as public, restricted, or private. Supports the use of an external controlled vocabulary.


isPreferred
###########
Set to "true" if this is the preferred address.




Graph
=====

.. graphviz:: /images/graph/ComplexDataTypes/ElectronicMessageSystem.dot