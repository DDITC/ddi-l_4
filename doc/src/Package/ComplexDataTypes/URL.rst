.. _URL:

URL
***
A web site URL



Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "isPreferred", "xs:boolean", "0..1"
   "content", "xs:anyURI", "1..1"
   "typeOfWebsite", "CodeValueType", "0..1"
   "effectivePeriod", "Date", "0..1"
   "privacy", "CodeValueType", "0..1"


isPreferred
###########
Set to "true" if this is the preferred URL.


content
#######
The content of the URL


typeOfWebsite
#############
The type of URL for example personal, project, organization, division, etc.


effectivePeriod
###############
The period for which this URL is valid.


privacy
#######
Indicates the privacy level of this URL




Graph
=====

.. graphviz:: /images/graph/ComplexDataTypes/URL.dot