.. _StandardKeyValuePair:

StandardKeyValuePair
********************
A basic data representation for computing systems and applications expressed as a tuple (attribute key, value). Attribute keys may or may not be unique.



Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "attributeKey", "CodeValueType", "0..1"
   "attributeValue", "CodeValueType", "0..1"


attributeKey
############
This key (sometimes referred to as a name) expressed as a string. Supports the use of an external controlled vocabulary which is the recommended approach.


attributeValue
##############
The value assigned to the named Key expressed as a string. Supports the use of an external controlled vocabulary.




Graph
=====

.. graphviz:: /images/graph/ComplexDataTypes/StandardKeyValuePair.dot