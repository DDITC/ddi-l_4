.. _InternationalCodeValueType:

InternationalCodeValueType
**************************
Allows for string content which may be taken from an externally maintained controlled vocabulary (code value). If the content is from a controlled vocabulary provide the code value, as well as a reference to the code list from which the value is taken. This differs from a CodeValue only by the provision of a language-location specification. DDI uses the International CodeValue in cases where controlled vocabularies are assumed to be highly language specific, such as nationally maintained subject headings, thesauri, or keywords derived from the content of documents. The ability to identify language is especially important when supporting searches by external language-specific search engines. Provide as many of the identifying attributes as needed to adequately identify the controlled vocabulary.

Extends
=======
:ref:`String`


Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "codeListID", "xs:string", "0..1"
   "codeListName", "xs:string", "0..1"
   "codeListAgencyName", "xs:string", "0..1"
   "codeListVersionID", "xs:string", "0..1"
   "otherValue", "xs:string", "0..1"
   "codeListURN", "xs:string", "0..1"
   "codeListSchemeURN", "xs:string", "0..1"


codeListID
##########
The ID of the code list (controlled vocabulary) that the content was taken from.


codeListName
############
The name of the code list.


codeListAgencyName
##################
The name of the agency maintaining the code list.


codeListVersionID
#################
The version number of the code list (default is 1.0).


otherValue
##########
If the value of the string is "Other" or the equivalent from the codelist, this attribute can provide a more specific value not found in the codelist.


codeListURN
###########
The URN of the codelist.


codeListSchemeURN
#################
If maintained within a scheme, the URN of the scheme containing the codelist.




Graph
=====

.. graphviz:: /images/graph/ComplexDataTypes/InternationalCodeValueType.dot