.. _LocationName:

LocationName
************
Name of the location using the DDI Name structure and the ability to add an effective date.

Extends
=======
:ref:`Name`


Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "effectivePeriod", "Date", "0..1"


effectivePeriod
###############
The time period for which this name is accurate and in use.




Graph
=====

.. graphviz:: /images/graph/ComplexDataTypes/LocationName.dot