.. _Name:

Name
****
A reusable type assigned to an element with the naming convention XxxName e.g. OrganizationName at selected locations where the element may be assumed to be administered by a registry or is otherwise shared. This is a human understandable name (word, phrase, or mnemonic) that reflects the ISO/IEC 11179-5 naming principles. An item administered by a registry should have at least one name. Names within an administered registry should follow the naming conventions of the registry. If more than one name is provided the context of each name should be noted and one name selected as the preferred name. See ISO/IEC 11179-5 Information Technology - Metadata Registries (MDR) Part 5: naming and identification principles. ISO/IEC1179-5:2005(E).

Extends
=======
:ref:`InternationalString`


Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "isPreferred", "xs:boolean", "0..1"
   "context", "xs:string", "0..1"


isPreferred
###########
If more than one name for the object is provided, use the isPreferred attribute to indicate which is the preferred name content. All other names should be set to isPreferred="false".


context
#######
A name may be specific to a particular context, i.e., a type of software, or a section of a registry. Identify the context related to the specified name.




Graph
=====

.. graphviz:: /images/graph/ComplexDataTypes/Name.dot