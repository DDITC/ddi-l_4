.. _Range:

Range
*****
Indicates the range of items expressed as a string, such as an alphabetic range.



Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "rangeUnit", "xs:string", "0..1"
   "minimumValue", "RangeValue", "0..1"
   "maximumValue", "RangeValue", "0..1"


rangeUnit
#########
Specifies the units in the range.


minimumValue
############
Minimum value in the range.


maximumValue
############
Maximum value in the range.




Graph
=====

.. graphviz:: /images/graph/ComplexDataTypes/Range.dot