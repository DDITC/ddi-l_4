.. _LineParameter:

LineParameter
*************
Specification of the line and offset for the beginning and end of the segment.



Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "startLine", "xs:integer", "0..1"
   "startOffset", "xs:integer", "0..1"
   "endLine", "xs:integer", "0..1"
   "endOffset", "xs:integer", "0..1"


startLine
#########
Number of lines from beginning of the document.


startOffset
###########
Number of characters from start of the line specified in StartLine.


endLine
#######
Number of lines from beginning of the document.


endOffset
#########
Number of characters from the start of the line specified in EndLine.




Graph
=====

.. graphviz:: /images/graph/ComplexDataTypes/LineParameter.dot