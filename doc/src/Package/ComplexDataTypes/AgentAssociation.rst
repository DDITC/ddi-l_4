.. _AgentAssociation:

AgentAssociation
****************
A basic structure for declaring the name of an Agent inline, reference to an Agent, and role specification. This object is used primarily within Annotation.




Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "agent", "BibliographicName", "0..1"
   "role", "PairedCodeValueType", "0..n"


agent
#####
Full name of the contributor. Language equivalents should be expressed within the International String structure.


role
####
The role of the of the Agent within the context of the parent property name with information on the extent to which the role applies. Allows for use of external controlled vocabularies. Reference should be made to the vocabulary within the structure of the role.




Graph
=====

.. graphviz:: /images/graph/ComplexDataTypes/AgentAssociation.dot