.. _LiteralText:

LiteralText
***********
Literal (static) text to be used in the instrument using the StructuredString structure plus an attribute allowing for the specification of white space to be preserved.

Extends
=======
:ref:`TextContent`


Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "text", "Text", "0..1"


text
####
The value of the static text string. Supports the optional use of XHTML formatting tags within the string structure. If the content of a literal text contains more than one language, i.e. "What is your understanding of the German word 'Gesundheit'?", the foreign language element should be placed in a separate LiteralText component with the appropriate xmlang value and, in this case, isTranslatable set to "false". If the existence of white space is critical to the understanding of the content (such as inclusion of a leading or trailing white space), set the attribute of Text xmspace to "preserve".




Graph
=====

.. graphviz:: /images/graph/ComplexDataTypes/LiteralText.dot