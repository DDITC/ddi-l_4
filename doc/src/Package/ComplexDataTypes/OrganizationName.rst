.. _OrganizationName:

OrganizationName
****************
Names by which the organization is known. Use the attribute isFormal="true" to designate the legal or formal name of the Organization. The preferred name should be noted with the isPreferred attribute. Names may be typed with TypeOfOrganizationName to indicate their appropriate usage.

Extends
=======
:ref:`Name`


Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "abbreviation", "InternationalString", "0..1"
   "typeOfOrganizationName", "CodeValueType", "0..1"
   "effectivePeriod", "Date", "0..1"
   "isFormal", "xs:boolean", "0..1"


abbreviation
############
An abbreviation or acronym for the name. This may be expressed in multiple languages. It is assumed that if only a single language is provided that it may be used in any of the other languages within which the name itself is expressed.


typeOfOrganizationName
######################
The type of organization name provided. the use of a controlled vocabulary is strongly recommended. At minimum this should include, e.g. PreviousFormalName, Nickname (or CommonName), Other.


effectivePeriod
###############
The time period for which this name is accurate and in use.


isFormal
########
The legal or formal name of the organization should have the isFormal attribute set to true. To avoid confusion only one organization name should have the isFormal attribute set to true. Use the TypeOfOrganizationName to further differentiate the type and applied usage when multiple names are provided.




Graph
=====

.. graphviz:: /images/graph/ComplexDataTypes/OrganizationName.dot