.. _AnnotationDate:

AnnotationDate
**************
A generic date type for use in Annotation which provides that standard date structure plus a property to define the date type (Publication date,  Accepted date, Copyrighted date, Submitted date, etc.). Equivilent of http://purl.org/dc/elements/1.1/date where the type of date may identify the Dublin Core refinement term.

Extends
=======
:ref:`Date`


Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "typeOfDate", "CodeValueType", "0..n"


typeOfDate
##########
Use to specify the type of date. This may reflect the refinements of dc:date such as dateAccepted, dateCopyrighted, dateSubmitted, etc.




Graph
=====

.. graphviz:: /images/graph/ComplexDataTypes/AnnotationDate.dot