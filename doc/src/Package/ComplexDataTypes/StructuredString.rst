.. _StructuredString:

StructuredString
****************
Packaging structure for multiple language versions of the same string content, for objects that allow for internal formatting using XHTML tags. Where an element of this type is repeatable, the expectation is that each repetition contains different content, each of which can be expressed in multiple languages.



Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "content", "Content", "1..n"


content
#######
Supports the optional use of XHTML formatting tags within the string structure. In addition to the language designation and information regarding translation, the attribute isPlain can be set to true to indicate that the content should be treated as plain unstructured text, including any XHTML formatting tags. Repeat the content element to provide multiple language versions of the same content.




Graph
=====

.. graphviz:: /images/graph/ComplexDataTypes/StructuredString.dot