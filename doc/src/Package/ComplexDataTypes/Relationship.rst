.. _Relationship:

Relationship
************
Relationship specification between this item and the item to which it is related. Provides a reference to any identifiable object and a description of the relationship.



Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "relationshipDescription", "StructuredString", "0..1"


relationshipDescription
#######################
A description of the nature of the relationship between the parent element of the relationship item and the DDI object to which it is related.




Graph
=====

.. graphviz:: /images/graph/ComplexDataTypes/Relationship.dot