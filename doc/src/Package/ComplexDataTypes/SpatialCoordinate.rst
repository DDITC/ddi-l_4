.. _SpatialCoordinate:

SpatialCoordinate
*****************
Lists the value and format type for the coordinate value. Note that this is a single value (X coordinate or Y coordinate) rather than a coordinate pair.



Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "coordinateValue", "xs:string", "0..1"
   "coordinateType", "PointFormat", "1..1"


coordinateValue
###############
The value of the coordinate expressed as a string.


coordinateType
##############
Identifies the type of point coordinate system using a controlled vocabulary. Point formats include decimal degree, degrees minutes seconds, decimal minutes, meters, and feet.




Graph
=====

.. graphviz:: /images/graph/ComplexDataTypes/SpatialCoordinate.dot