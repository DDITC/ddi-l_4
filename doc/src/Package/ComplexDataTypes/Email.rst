.. _Email:

Email
*****
An e-mail address which conforms to the internet format (RFC 822) including its type and time period for which it is valid.



Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "internetEmail", "xs:string", "0..1"
   "typeOfEmail", "CodeValueType", "0..1"
   "effectivePeriod", "Date", "0..1"
   "privacy", "CodeValueType", "0..1"
   "isPreferred", "xs:boolean", "0..1"


internetEmail
#############
The email address expressed as a string (should follow the Internet format specification - RFC 5322) e.g. user@server.ext, more complex and flexible examples are also supported by the format.


typeOfEmail
###########
Code indicating the type of e-mail address. Supports the use of an external controlled vocabulary. (e.g. home, office)


effectivePeriod
###############
Time period for which the e-mail address is valid.


privacy
#######
Indicates the level of privacy


isPreferred
###########
Set to true if this is the preferred email




Graph
=====

.. graphviz:: /images/graph/ComplexDataTypes/Email.dot