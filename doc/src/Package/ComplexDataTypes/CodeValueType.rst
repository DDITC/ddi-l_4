.. _CodeValueType:

CodeValueType
*************
Allows for string content which may be taken from an externally maintained controlled vocabulary (code value). If the content is from a controlled vocabulary provide the code value, as well as a reference to the code list from which the value is taken. Provide as many of the identifying attributes as needed to adequately identify the controlled vocabulary. Note that DDI has published a number of controlled vocabularies applicable to several locations using the CodeValue structure. Use of shared controlled vocabularies helps support interoperability and machine actionability.



Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "codeValue", "xs:string", "1..1"
   "codeListID", "xs:string", "0..1"
   "codeListName", "xs:string", "0..1"
   "codeListAgencyName", "xs:string", "0..1"
   "codeListVersionID", "xs:string", "0..1"
   "otherValue", "xs:string", "0..1"
   "codeListURN", "xs:string", "0..1"
   "codeListSchemeURN", "xs:string", "0..1"


codeValue
#########
The actual value.


codeListID
##########
The ID of the code list (controlled vocabulary) that the content was taken from.


codeListName
############
The name of the code list.


codeListAgencyName
##################
The name of the agency maintaining the code list.


codeListVersionID
#################
The version number of the code list (default is 1.0).


otherValue
##########
If the value of the string is "Other" or the equivalent from the codelist, this attribute can provide a more specific value not found in the codelist.


codeListURN
###########
The URN of the codelist.


codeListSchemeURN
#################
If maintained within a scheme, the URN of the scheme containing the codelist.




Graph
=====

.. graphviz:: /images/graph/ComplexDataTypes/CodeValueType.dot