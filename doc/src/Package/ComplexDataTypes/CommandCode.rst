.. _CommandCode:

CommandCode
***********
Contains information on the command used for processing data. Contains a description of the command which should clarify for the user the purpose and process of the command, an in-line provision of the command itself, a reference to an external version of the command such as a coding script, and the option for attaching an extension to DDI to permit insertion of a command code in a foreign namespace. The definition of the InParameter, OutParameter, and Binding declared within CommandCode are available for use by all formats of the command.



Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "description", "StructuredString", "0..1"
   "binding", "Binding", "0..n"


description
###########
A description of the purpose and use of the command code provided. Supports multiple languages.


binding
#######
Defines the link between the OutParameter of an external object to an InParameter of this CommandCode.




Graph
=====

.. graphviz:: /images/graph/ComplexDataTypes/CommandCode.dot