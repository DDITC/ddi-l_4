.. _AudioSegment:

AudioSegment
************
Describes the type and length of the audio segment.



Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "typeOfAudioClip", "CodeValueType", "0..1"
   "audioClipBegin", "xs:string", "0..1"
   "audioClipEnd", "xs:string", "0..1"


typeOfAudioClip
###############
The type of audio clip provided. Supports the use of a controlled vocabulary.


audioClipBegin
##############
The point to begin the audio clip. If no point is provided the assumption is that the start point is the beginning of the clip provided.


audioClipEnd
############
The point to end the audio clip. If no point is provided the assumption is that the end point is the end of the clip provided.




Graph
=====

.. graphviz:: /images/graph/ComplexDataTypes/AudioSegment.dot