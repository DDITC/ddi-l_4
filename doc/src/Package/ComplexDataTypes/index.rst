****************
ComplexDataTypes
****************
Extensions of base type Primitives
Contents 

.. toctree::
   :maxdepth: 2

   AccessLocation
   Address
   AgentAssociation
   AgentId
   AnnotationDate
   AudioSegment
   BasedOnObject
   BibliographicName
   CharacterOffset
   CodeValueType
   CommandCode
   ConditionalText
   ContactInformation
   Content
   ContentDateOffset
   CorrespondenceType
   CountryCode
   Date
   DynamicText
   ElectronicMessageSystem
   Email
   Form
   HistoricalDate
   Image
   ImageArea
   IndividualName
   InternationalCodeValueType
   InternationalIdentifier
   InternationalString
   Label
   LineParameter
   LiteralText
   LocalId
   LocationName
   Name
   OrganizationName
   PairedCodeValueType
   Point
   Polygon
   PrivateImage
   ProprietaryInfo
   Range
   RangeValue
   ReferenceDate
   Relationship
   ResourceIdentifier
   Segment
   Software
   SpatialCoordinate
   SpecificSequence
   StandardKeyValuePair
   Statistic
   String
   StructuredString
   Telephone
   Text
   TextContent
   TextualSegment
   URI
   URL
   Value
   VideoSegment
   XMLPrefixMap
