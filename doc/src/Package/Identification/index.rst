**************
Identification
**************
Objects to support Identification and Versioning
Contents 

.. toctree::
   :maxdepth: 2

   AnnotatedIdentifiable
   Identifiable
