.. _Identifiable:

Identifiable
************
Used to identify objects for purposes of internal and/or external referencing. Elements of this type are versioned. Most objects except for the ComplexDataTypes will inherit from Identifiable or the more specialised AnnotatedIdentfiable.



Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "agency", "xs:string", "1..1"
   "id", "xs:string", "1..1"
   "version", "xs:string", "1..1"


agency
######
This is the registered agency code with optional sub-agencies separated by dots. For example, diw.soep, ucl.qss, abs.essg.


id
##
The ID of the object. This must conform to the allowed structure of the DDI Identifier and must be unique within the declared scope of uniqueness (Agency or Maintainable).


version
#######
The version number of the object. The version number is incremented whenever the non-administrative metadata contained by the object changes.




Graph
=====

.. graphviz:: /images/graph/Identification/Identifiable.dot