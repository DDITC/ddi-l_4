.. _AnnotatedIdentifiable:

AnnotatedIdentifiable
*********************
Used to identify objects for purposes of internal and/or external referencing. Elements of this type are versioned. Provides administrative metadata about the object in addition to what is provided by Identifiable, including more details on the versioning of the object. Most objects except for the ComplexDataTypes will inherit AnnotatedIdentfiable.

Extends
=======
:ref:`Identifiable`


Properties
==========

.. csv-table:: 
   :header: "Name", "Type", "Cardinality"
   
   "versionResponsibility", "xs:string", "0..1"
   "versionRationale", "xs:string", "0..1"
   "versionDate", "xs:dateTime", "0..1"
   "isUniversallyUnique", "xs:boolean", "1..1"
   "isPersistent", "xs:boolean", "1..1"
   "localId", "LocalId", "0..n"
   "basedOnObject", "BasedOnObject", "0..1"


versionResponsibility
#####################
Contributor who has the ownership and responsiblity for the current version.


versionRationale
################
The reason for making this version of the object.


versionDate
###########
The date and time the object was changed.


isUniversallyUnique
###################
Usually the combination of agency and id (ignoring different versions) is unique. If isUniversallyUnique is set to true, it indicates that the id itsef is universally unique (unique across systems and/or agencies) and therefore the agency part is not required to ensure uniqueness. Default value is false.


isPersistent
############
Usually the content of the current version is allowed to change, for example as the contibutor is working on the object contents. However, when isPersistent is true, it indicates the there will be no more changes to the current version. Default value is false.


localId
#######
This is an identifier in a given local context that uniquely references an object, as opposed to the full ddi identifier which has an agency plus the id. For example, localId could be a variable name in a dataset.


basedOnObject
#############
The object/version that this object version is based on.




Graph
=====

.. graphviz:: /images/graph/Identification/AnnotatedIdentifiable.dot