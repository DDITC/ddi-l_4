********
Packages
********

.. toctree::
   :caption: List of packages
   :maxdepth: 2

   NewObjectsForSimpleInstruments/index.rst
   Identification/index.rst
   Discovery/index.rst
   Primitives/index.rst
   Processing/index.rst
   Utility/index.rst
   SimpleDiscovery/index.rst
   Representations/index.rst
   DDIUtility/index.rst
   DDIDocument/index.rst
   Comparison/index.rst
   Collections/index.rst
   BaseObjects/index.rst
   ComplexDataTypes/index.rst
   Conceptual/index.rst
   DataCapture/index.rst
   Correspondences/index.rst
   CoreProcess/index.rst
   Agents/index.rst