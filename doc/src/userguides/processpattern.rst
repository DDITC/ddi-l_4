Using the Process pattern
==========================

The process pattern consists of classes to describe business functions and workflows that can be mapped to process execution languages like BPEL or BPMN.
A Process is a Sequence of Process Steps that perform one or more business functions. Each Process Step can be performed by a Service. There are two types of Process Steps: Acts and Control Constructs. Acts represent actions and are atomic Process Steps, i.e. they cannot be composed of other Process Steps. An Act is similar to an instruction in a programming language and a terminal in the production rules of a formal grammar. A Control Construct describes logical flows between Process Steps.

**Figure 1. Process Step class**

.. |figure1| image:: ../images/ddi4_process01.png

|figure1|

Process Steps can be nested and thus describe processes at multiple levels of detail. The nesting of Process Steps always terminate in an Act. All nesting of Process Steps occur via Sequences, a specialization of Control Constructs.
Control Constructs include Sequence and Conditional Control Construct (Figure 2). The former models linear execution of Process Steps whereas the latter includes three types of iterative constructs: repeatWhile, repeatUntil and Loop. The Sequence at the end of the contains association represents the body of the Conditional Control Construct, which is executed depending on the result of the condition evaluation. The specialized sub-classes determine whether the Sequence is executed in each iteration before the condition is evaluated (RepeatUntil), or after (RepeatWhile, Loop). The Loop also provides a counter with initialValue and stepValue that can be used to specify in the condition how many times the Sequence in the body is executed.

**Figure 2. Control Construct class**

.. |figure2| image:: ../images/ddi4_process02.png

|figure2|

In addition to the iterative constructs, Conditional Control Constructs includes IfThenElse, which provides a means to specify branching control flows. It contains the condition inherited from the parent class and two associations: contains (also inherited from the parent class), to the Sequence of Process Steps that is executed when the condition is true, and containsElse, to an optional Sequence to be executed if the condition is evaluated to false. Optionally, IfThenElse can also have an associated ElseIf construct to model switch statements.
It is important to note that the model also covers parallel processing: Conditional Control Constructs can contain multiple Sequences that could be executed in parallel (more on this later).
A Sequence can be viewed as a Collection whose Members are Process Steps that can be ordered in three different ways: with a Sequence Order (traditional design-time total ordering), with one or more Temporal Interval Relations (design-time temporal constraint or “fuzzy” ordering), or with a Rule (constructor) to determine ordering at run-time.
We discuss Sequence Order first. A Sequence Order realizes Collection pattern as follows (Figure 3).

**Figure 3. Sequence Order**

.. |figure3| image:: ../images/ddi4_process03.png

|figure3|

Sequence Order and Sequence Order Pair realize Strict Order Relation and Ordered Pair, respectively. Let us remember from the Binary Relations Specialization diagram [hyperlink to Collections Figure ???] that Strict Order Relation is an Asymmetric Binary Relation that is ANTI_SYMMETRIC, ANTI_REFLEXIVE and TRANSITIVE. In addition, the Sequence Order realization has totality=TOTAL and semantics=SUCCESSOR_OF, which means that it can specify a total order of the Process Steps in the Sequence where the order semantics is given by SUCCESOR_OF.
We discuss next Temporal Interval Relations. They provide a mechanism for capturing Allen’s interval relations, one of the best established formalisms for temporal reasoning. Temporal Interval Relations can be used to define temporal constraints between pairs of Process Steps, e.g. whether the execution of two Process Steps can overlap in time or not, or one has to finish before the other one starts, etc. This also supports parallel processing.
There are thirteen Temporal Interval Relations: twelve asymmetric ones, i.e. precedes, meets, overlaps, finishes, contains, starts and their converses, plus equals, which is the only one that has no converse, or rather, it is the same as its converse. Together these relations are distinct (any pair of definite intervals are described by one and only one of the relations), exhaustive (any pair of definite intervals are described by one of the relations), and qualitative (no numeric time spans are considered).
Following Allen’s, Temporal Interval Relations are defined as follows.

**Figure 4. Allen's Temporal Interval Relations**

.. |figure4| image:: ../images/ddi4_process04.png

|figure4|

In DDI-Views, each one of the asymmetric Allen’s interval relations is a Temporal Interval Relation that realizes different Binary Relations with specific temporal semantics. All asymmetric Temporal Interval Relation contains Ordered Interval Pairs whereas the only symmetric one, i.e. Equals, contains Unordered Interval Pairs (Figure 5). For instance, the Precedes Interval Relation realizes the pattern as follows.

**Figure 5. Precedes Interval Relation**

.. |figure5| image:: ../images/ddi4_process05.png

|figure5|

Precedes Interval Relation and Ordered Interval Pair realize Strict Order Relation and Ordered Pair, respectively. If we look back to the Binary Relations Specialization diagram in the previous section we notice that Strict Order Relation has the TEMPORAL_PRECEDES semantics, among others, which means that the Process Step at the end of the source association in the Ordered Interval Pair has to finish before the one at the end of target starts.
The Equals Interval Relation (Figure 6) is a slightly different case because it is an equivalence relation rather than an asymmetric one and therefore it contains Unordered Interval Pairs.

**Figure 6. Equals Interval Relation**

.. |figure6| image:: ../images/ddi4_process06.png

|figure6|

Equals Interval Relation and Unordered Interval Pair realize Equivalence Relation and Unordered Pair, respectively. Equivalence Relation is simply a Symmetric Binary Relation that is REFLEXIVE and TRANSITIVE with some additional semantics, among which we find TEMPORAL_EQUALS, the one required by the Equals Interval Relation. This means that the execution of the two Process Steps at the end of the maps association in Unordered Interval Pair begin and end at the same time.
Temporal Interval Relations and Sequence Orders can be combined in the same Sequence. For instance, consider Figure 7:

**Figure 7. Combination of Interval Relations**

.. |figure7| image:: ../images/ddi4_process07.png

|figure7|

Question Q2 requires the answer of question Q1 so it has to be executed after Q1. That is an example of the traditional sequence ordering given by the Sequence Order Relation. However, note that there is no dependency between Q2 and Q3 since both require only the answer of Q1. Therefore Q2 and Q3 could be executed at the same time, which can be expressed with the Equals Interval Relation.
