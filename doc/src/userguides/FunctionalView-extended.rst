Functional View
================


Definition
-----------

A sub-set of the DDI class Library selected to address a common Use Case(s) within a Target Audience(s).


Purpose
-------

A primary goal of a Functional View is to reduce complexity in the documentation and bindings of the view as compared to the whole standard. At the same time it is desirable to have the maximal degree of compatibility among views. Several views may be applied to a given collection of data through the data�s lifecycle. Ideally metadata accumulated through the lifecycle should move smoothly through the views without loss.  
A view should be defined with very broad use case in mind, e.g. methodology, classification, discovery. More specific use cases (e.g. those usually associated with profiles) within the broad category of a view will be addressed in the documentation, e.g. which properties to include, which ones to exclude, how classes in the view should be used, etc.


Requirements
------------

1. Clear definition of the purpose (Use Cases and Target Audience) for each Functional View
#. Clear definition of which classes are included and their specific use within the Functional View
#. Clear head class that allows identification of all included classes within the Functional View and all included objects in an instance of that view. This is the head class of the Functional View but there may be 1..n instances. 


Structure
---------

1. A Functional View will be a subset of classes from the Library. They will be included with no restrictions or extensions. 
#. All classes will retain the DDI Library namespace.
#. Documentation will be used to clarify what the properties and relationships of a class are relevant or �core� to the Use Case of the Functional View. The target class of all required relationships should be included in the �core�.
#. Any reference from an included class to a class NOT contained in a Functional View will be defined as an external reference. It is the responsibility of the user to ensure that the referenced class is available in a useable format for the particular instance (i.e. similar binding). The user should assume that someone else supporting this view will NOT be able to manage the external content. If this content is essential to the instance then the use of the specific Functional View should be reconsidered.


Content of Structure
--------------------

1. All classes to be included in the Functional View. Note that any classes not specifically included in the Functional View but acting as the target a relationship in an included class will be considered an external reference.
#. Certain classes of Functional Views may have recommended content to ensure consistency in discovery and/or self-descriptive documentation of an instance of the Functional View. For the purpose of the Prototype a required Document Information with the minimal content of an identification for the instance has been added to the Functional View.
#. A Functional View must have a required class that will serve as a starting point for entry into an object model hierarchy identifying all the component classes that make up the Functional View


Documentation in the Definition of a Functional View
-----------------------------------------------------

1. A Functional View will contain a description of the Target Audience and Uses Cases addressed by the Functional View
#. Documentation on use of restricted classes within the context of the Functional View is provided.


Applied Use of Functional Views
-------------------------------

1. Common core information (as defined by the Functional View based on DDI guidelines) should be completed if the binding is to be shared, in all or part, with other systems. This includes access to the instance or its contents in external search or retrieval systems.
2. Extensions of the Functional View beyond the documented usage of the classes with the Functional View should FIRST use the existing properties and relationships within the class.
3. When there is a need to restrict a Functional View it should be done on the instance level (i.e. like a DDI-L profile).
