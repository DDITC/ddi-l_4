************
User Guides
************

.. toctree::
   :maxdepth: 2

   collectionpattern.rst
   processpattern.rst
   variablecascade.rst
