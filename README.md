# README #

This repository contains the dated development draft review releases of DDI-L 4. Each deposit will be copy of the zip file provided for review for additional information on the coverage of a review release see the [DDI 4 Comment and Review Page](https://ddi-alliance.atlassian.net/wiki/display/DDI4/DDI+4+-+Comment+and+Review).

[Download the 2015 Q1 Review package](https://bitbucket.org/DDITC/ddi-l_4/downloads/DDI4_Q1_review_2015-04-23.zip)

[Model Documentation Preview Option 1](http://ddi4.readthedocs.org)